const express = require('express')
const app = express()
const port = 3000
const bodyParser = require('body-parser');

let volume = 10;
let standbyTime = 10;
let newTime = "05:55"
let newDate = "1970-01-01"
let alarm = "05:43"

let volumeText = 10;
let standbyText = 10;
let timeText = "05:55"
let newDateText = "1970-01-01"
let alarmText = "05:22"

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs')

app.get('/', function (req, res) {
    res.render('index' ,{volume: volumeText, standbyTime: standbyText, newTime: timeText, newDate: newDateText, alarm: alarmText});
})

app.get('/data', function (req, res) {
    res.render('data' ,{volume: volumeText, standbyTime: standbyText, newTime: timeText, newDate: newDateText, alarm: alarmText});
})

app.post('/volume', function (req, res) {
    volume = req.body.volume;
    volumeText = `${volume}`;
    res.render('index', {volume: volumeText, standbyTime: standbyText, newTime: timeText, newDate: newDateText, alarm: alarmText});
})

app.post('/time', function (req, res) {
    newTime = req.body.timeInput;
    timeText = `${newTime}`;
    res.render('index', {volume: volumeText, standbyTime: standbyText, newTime: timeText, newDate: newDateText, alarm: alarmText});
})

app.post('/standby', function (req, res) {
    standbyTime = req.body.standbyTime;
    standbyText = `${standbyTime}`;
    res.render('index', {volume: volumeText, standbyTime: standbyText, newTime: timeText, newDate: newDateText, alarm: alarmText});
})

app.post('/date', function (req, res) {
    newDate = req.body.dateInput;
    newDateText = `${newDate}`;
    res.render('index', {volume: volumeText, standbyTime: standbyText, newTime: timeText, newDate: newDateText, alarm: alarmText});
})

app.post('/alarm', function (req, res) {
    alarm = req.body.alarmInput;
    alarmText = `${alarm}`;
    res.render('index', {volume: volumeText, standbyTime: standbyText, newTime: timeText, newDate: newDateText, alarm: alarmText});
})


app.listen(port, function () {
    console.log('Example app listening on port 3000!')
  })
  
