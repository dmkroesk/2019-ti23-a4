///*
 //* menu.c
 //*
 //* Created: 19/02/2019 11:06:37
 //*  Author: Tim van der Vooren
 //*/ 
#define LOG_MODULE  LOG_MAIN_MODULE
#define OK			1
#define NOK			0

#include <sys/thread.h>
#include <sys/timer.h>

#include "Application/menu.h"

#include "Control/log.h"
#include "Control/flashIO.h"
#include "Control/timeutils.h"
#include "Control/util.h"
#include "Control/userdata.h"
#include "Hardware/remcon.h"
#include "Control/shoutcast.h"
#include "Control/display.h"
#include "Control/alarm.h"
#include "Control/internetutils.h"
#include "Control/simon_says.h"
#include "Control/webinterface.h"
#include "Control/rss.h"

#include "Hardware/keyboard.h"
#include "Hardware/vs10xx.h"

static unsigned int currentMenuItem = 0;
static unsigned int currentMenuId;
static unsigned int previousMenuItem = 0;
static unsigned int previousMenuId;

static int powerState = 1;
static int standbyTime;
static int backlight = LCD_BACKLIGHT_ON;
static int volume;
static int volumeChanged = 0;

static int alarmOn = 0;
static int alarmTime;
static int snoozableAlarmTime;

static int dailyAlarms[] = {-1,-1,-1,-1,-1,-1,-1};
static int currentDayId = 0;

static char menuCurTime[12];
static char menuCurDate[10];

static char *standbyTimes[] = {"5 ", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55", "60"};

static int cursorPosition;

static int hours = 0;
static int minutesOne = 0;
static int minutesTwo = 0;
int sec, min, hour;
char* rss_title;

static int day = 1;
static int month = 1;
static int year = 2000;
static int daysPerMonth[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
static int weekDay = 0;

static int macHexArray[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
static int macAddress[] = {0x10,0x36,0x45,30,0x02,0x07};
	
int simonSaysActive = 0;
int inetStatus = 0;


//these are all the menu items 
MENU_ITEM menu_items[] = {
	{
		MENU_STANDBY,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			MENU_MAIN_1_ID,
			-1,
			-1,
			-1,
			-1,
			-1
		},
		{
			"HH:MM   DD-MM-YY",
			"[Esc] Menu      "
		},
		{
			NULL, NULL, NULL, NULL, NULL, NULL
		}
	},
	{
		MENU_MAIN_1_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			MENU_STANDBY,
			MODUS_SUB_1_ID,
			MENU_MAIN_3_ID,
			MENU_MAIN_2_ID,
			VOLUME_UP,
			VOLUME_DOWN
		},
		{
			"Menu       HH:MM",
			"< Modus        >"
		},
		{
			NULL, NULL, NULL, NULL, volumeUp, volumeDown
		}
	},
	{
		MENU_MAIN_2_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			MENU_STANDBY,
			SETTINGS_SUB_1_ID,
			MENU_MAIN_1_ID,
			MENU_MAIN_3_ID,
			VOLUME_UP,
			VOLUME_DOWN
		},
		{
			"Menu       HH:MM",
			"< Instellingen >"
		},
		{
			NULL, NULL, NULL, NULL, volumeUp, volumeDown
		}
	},
	{
		MENU_MAIN_3_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			MENU_STANDBY,
			-1,
			MENU_MAIN_2_ID,
			MENU_MAIN_1_ID,
			VOLUME_UP,
			VOLUME_DOWN
		},
		{
			"Menu       HH:MM",
			"< Simon Says   >"
		},
		{
			NULL, startSimonSays, NULL, NULL, volumeUp, volumeDown
		}
	},
	{
		MODUS_SUB_1_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			MENU_MAIN_1_ID,
			MENU_MAIN_1_ID,		
			MODUS_SUB_3_ID,
			MODUS_SUB_2_ID,
			VOLUME_UP,
			VOLUME_DOWN
		},
		{
			"Modus      HH:MM",
			"< Wekker       >"
		},
		{
			NULL, stopRadioStream, NULL, NULL, volumeUp, volumeDown
		}
	},
	{
		MODUS_SUB_2_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			MENU_MAIN_1_ID,
			MENU_MAIN_1_ID,		
			MODUS_SUB_1_ID,
			MODUS_SUB_3_ID,
			VOLUME_UP,
			VOLUME_DOWN
		},
		{
			"Modus      HH:MM",
			"< Radio        >"
		},
		{
			NULL, startRadioStream, NULL, NULL, volumeUp, volumeDown
		}
	},
	{
		MODUS_SUB_3_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			MENU_MAIN_1_ID,
			RSS_FEED,			//TODO: ZET MODUS NAAR RSS FEED
			MODUS_SUB_2_ID,
			MODUS_SUB_1_ID,
			VOLUME_UP,
			VOLUME_DOWN
		},
		{
			"Modus      HH:MM",
			"< RSS feed     >"
		},
		{
			NULL, NULL, NULL, NULL, volumeUp, volumeDown
		}
	},
	{
		SETTINGS_SUB_1_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			MENU_MAIN_2_ID,
			ALARM_SUB_1_ID,
			SETTINGS_SUB_6_ID,
			SETTINGS_SUB_2_ID,
			VOLUME_UP,
			VOLUME_DOWN
		},
		{
			"Instelling HH:MM",
			"< Wekker       >"
		},
		{
			NULL, NULL, NULL, NULL, volumeUp, volumeDown
		}
	},
	{
		SETTINGS_SUB_2_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			MENU_MAIN_2_ID,
			RADIO_SUB_1_ID,
			SETTINGS_SUB_1_ID,
			SETTINGS_SUB_3_ID,
			VOLUME_UP,
			VOLUME_DOWN
		},
		{
			"Instelling HH:MM",
			"< Radio        >"
		},
		{
			NULL, NULL, NULL, NULL, volumeUp, volumeDown
		}
	},
	{
		SETTINGS_SUB_3_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			MENU_MAIN_2_ID,
			NETWORK_SUB_1_ID,
			SETTINGS_SUB_2_ID,
			SETTINGS_SUB_4_ID,
			VOLUME_UP,
			VOLUME_DOWN
		},
		{
			"Instelling HH:MM",
			"< Netwerk      >"
		},
		{
			NULL, setCurrentMacAddressToDisplay, NULL, NULL, volumeUp, volumeDown
		}
	},
	{
		SETTINGS_SUB_4_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			MENU_MAIN_2_ID,
			TIME_SUB_1_ID,
			SETTINGS_SUB_3_ID,
			SETTINGS_SUB_5_ID,
			VOLUME_UP,
			VOLUME_DOWN
		},
		{
			"Instelling HH:MM",
			"< Tijd         >"
		},
		{
			NULL, NULL, NULL, NULL, volumeUp, volumeDown
		}
	},
	{
		SETTINGS_SUB_5_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			MENU_MAIN_2_ID,
			DATE_SUB_1_ID,
			SETTINGS_SUB_4_ID,
			SETTINGS_SUB_6_ID,
			VOLUME_UP,
			VOLUME_DOWN
		},
		{
			"Instelling HH:MM",
			"< Datum        >"
		},
		{
			NULL, setCurrentDateToDisplay, NULL, NULL, volumeUp, volumeDown
		}
	},
		{
			SETTINGS_SUB_6_ID,
			{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
				MENU_MAIN_2_ID,
				WEB_SUB_1_ID,
				SETTINGS_SUB_5_ID,
				SETTINGS_SUB_1_ID,
				VOLUME_UP,
				VOLUME_DOWN
			},
			{
				"Instelling HH:MM",
				"< Webinterface >"
			},
			{
				NULL, NULL, NULL, NULL, NULL, NULL
			}
		},
	{
		ALARM_SUB_1_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			SETTINGS_SUB_1_ID,
			ALARM_SUB_SUB_1_1_ID,
			ALARM_SUB_2_ID,
			ALARM_SUB_2_ID,
			VOLUME_UP,
			VOLUME_DOWN
		},
		{
			"Wekker     HH:MM",
			"< Dagelijks    >"
		},
		{
			NULL, setCurrentAlarmTimeToDisplay, NULL, NULL, volumeUp, volumeDown
		}
	},
	{
		ALARM_SUB_2_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			SETTINGS_SUB_1_ID,
			ALARM_SUB_SUB_2_1_ID,	
			ALARM_SUB_1_ID,
			ALARM_SUB_1_ID,
			VOLUME_UP,
			VOLUME_DOWN
		},
		{
			"Wekker     HH:MM",
			"< Alarm per dag>"
		},
		{
			NULL, NULL, NULL, NULL, NULL, NULL
		}
	},
	{
		ALARM_SUB_SUB_1_1_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			ALARM_SUB_1_ID,
			ALARM_SUB_1_ID,							
			-1,
			-1,
			-1,
			-1
		},
		{
			"Dagelijks alarm ",
			NULL
		},
		{
			NULL, saveAlarmTime, setCursorPositionLeft, setCursorPositionRight, increaseTime, decreaseTime
		}
	},
	{
		ALARM_SUB_SUB_2_1_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			ALARM_SUB_2_ID,
			ALARM_SUB_SUB_TIME_2_1_1_ID,							
			ALARM_SUB_SUB_2_7_ID,
			ALARM_SUB_SUB_2_2_ID,
			-1,
			-1
		},
		{
			"Alarm per dag   ",
			"Maandag         "
		},
		{
			NULL, SetCurrentDayID, NULL, NULL, NULL, NULL
		}
	},
	{
		ALARM_SUB_SUB_2_2_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			ALARM_SUB_2_ID,
			ALARM_SUB_SUB_TIME_2_1_1_ID,							
			ALARM_SUB_SUB_2_1_ID,
			ALARM_SUB_SUB_2_3_ID,
			-1,
			-1
		},
		{
			"Alarm per dag   ",
			"Dinsdag         "
		},
		{
			NULL, SetCurrentDayID, NULL, NULL, NULL, NULL
		}
	},
	{
		ALARM_SUB_SUB_2_3_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			ALARM_SUB_2_ID,
			ALARM_SUB_SUB_TIME_2_1_1_ID,							
			ALARM_SUB_SUB_2_2_ID,
			ALARM_SUB_SUB_2_4_ID,
			-1,
			-1
		},
		{
			"Alarm per dag   ",
			"Woensdag        "
		},
		{
			NULL, SetCurrentDayID, NULL, NULL, NULL, NULL
		}
	},
	{
		ALARM_SUB_SUB_2_4_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			ALARM_SUB_2_ID,
			ALARM_SUB_SUB_TIME_2_1_1_ID,							
			ALARM_SUB_SUB_2_3_ID,
			ALARM_SUB_SUB_2_5_ID,
			-1,
			-1
		},
		{
			"Alarm per dag   ",
			"Donderdag       "
		},
		{
			NULL, SetCurrentDayID, NULL, NULL, NULL, NULL
		}
	},
	{
		ALARM_SUB_SUB_2_5_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			ALARM_SUB_2_ID,
			ALARM_SUB_SUB_TIME_2_1_1_ID,							
			ALARM_SUB_SUB_2_4_ID,
			ALARM_SUB_SUB_2_6_ID,
			-1,
			-1
		},
		{
			"Alarm per dag   ",
			"Vrijdag         "
		},
		{
			NULL, SetCurrentDayID, NULL, NULL, NULL, NULL
		}
	},
	{
		ALARM_SUB_SUB_2_6_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			ALARM_SUB_2_ID,
			ALARM_SUB_SUB_TIME_2_1_1_ID,							
			ALARM_SUB_SUB_2_5_ID,
			ALARM_SUB_SUB_2_7_ID,
			-1,
			-1
		},
		{
			"Alarm per dag   ",
			"Zaterdag        "
		},
		{
			NULL, SetCurrentDayID, NULL, NULL, NULL, NULL
		}
	},
	{
		ALARM_SUB_SUB_2_7_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			ALARM_SUB_2_ID,
			ALARM_SUB_SUB_TIME_2_1_1_ID,							
			ALARM_SUB_SUB_2_6_ID,
			ALARM_SUB_SUB_2_1_ID,
			-1,
			-1
		},
		{
			"Alarm per dag   ",
			"Zondag          "
		},
		{
			NULL, SetCurrentDayID, NULL, NULL, NULL, NULL
		}
	},
	{
		ALARM_SUB_SUB_TIME_2_1_1_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			ALARM_SUB_2_ID,
			ALARM_SUB_2_ID,							
			-1,
			-1,
			-1,
			-1
		},
		{
			"Tijd instellen  ",
			NULL
		},
		{
			NULL, SetDayAlarm, setCursorPositionLeft, setCursorPositionRight, increaseTime, decreaseTime
		}
	},
	{
		RADIO_SUB_1_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			SETTINGS_SUB_2_ID,
			-1,						//TO DO radio wordt aangepast
			RADIO_SUB_5_ID,
			RADIO_SUB_2_ID,
			VOLUME_UP,
			VOLUME_DOWN
		},
		{
			"Radio      HH:MM",
			"< Jazz         >"
		},
		{
			NULL, changeRadioStation, NULL, NULL, volumeUp, volumeDown
		}
	},
	{
		RADIO_SUB_2_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			SETTINGS_SUB_2_ID,
			-1,						//TO DO radio wordt aangepast
			RADIO_SUB_1_ID,
			RADIO_SUB_3_ID,
			VOLUME_UP,
			VOLUME_DOWN
		},
		{
			"Radio      HH:MM",
			"< Classic rock >"
		},
		{
			NULL, changeRadioStation, NULL, NULL, volumeUp, volumeDown
		}
	},
	{
		RADIO_SUB_3_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			SETTINGS_SUB_2_ID,
			-1,							//TO DO radio wordt aangepast
			RADIO_SUB_2_ID,
			RADIO_SUB_4_ID,
			VOLUME_UP,
			VOLUME_DOWN
		},
		{
			"Radio      HH:MM",
			"< Aardschock   >"
		},
		{
			NULL, changeRadioStation, NULL, NULL, volumeUp, volumeDown
		}
	},
	{
		RADIO_SUB_4_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			SETTINGS_SUB_2_ID,
			-1,						//TO DO radio wordt aangepast
			RADIO_SUB_3_ID,
			RADIO_SUB_5_ID,
			VOLUME_UP,
			VOLUME_DOWN
		},
		{
			"Radio      HH:MM",
			"< Slam.fm      >"
		},
		{
			NULL, changeRadioStation, NULL, NULL, volumeUp, volumeDown
		}
	},
	{
		RADIO_SUB_5_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			SETTINGS_SUB_2_ID,
			-1,							//TO DO radio wordt aangepast
			RADIO_SUB_4_ID,
			RADIO_SUB_1_ID,
			VOLUME_UP,
			VOLUME_DOWN
		},
		{
			"Radio      HH:MM",
			"< Qmusic       >"
		},
		{
			NULL, changeRadioStation, NULL, NULL, volumeUp, volumeDown
		}
	},
	{
		NETWORK_SUB_1_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			SETTINGS_SUB_3_ID,
			SETTINGS_SUB_3_ID,
			-1,
			-1,
			-1,
			-1
		},
		{
			NULL,
			NULL
		},
		{
			NULL, saveMacAddressSettings, setCursorPositionLeft, setCursorPositionRight, increaseMacAddress, decreaseMacAddress
		}
	},
	{
		TIME_SUB_1_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			SETTINGS_SUB_4_ID,
			TIME_SUB_SUB_1_1_ID,				
			TIME_SUB_3_ID,
			TIME_SUB_3_ID,
			VOLUME_UP,
			VOLUME_DOWN
		},
		{
			"Tijd       HH:MM",
			"< Tijd         >"
		},
		{
			NULL, setCurrentTimeToDisplay, NULL, NULL, volumeUp, volumeDown
		}
	},
	{
		TIME_SUB_3_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			SETTINGS_SUB_4_ID,
			TIME_SUB_SUB_3_1_ID,				
			TIME_SUB_1_ID,
			TIME_SUB_1_ID,
			VOLUME_UP,
			VOLUME_DOWN
		},
		{
			"Tijd       HH:MM",
			"< Standby      >"
		},
		{
			NULL, setStandbyTimeToDisplay, NULL, NULL, volumeUp, volumeDown
		}
	},
	{
		TIME_SUB_SUB_1_1_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			TIME_SUB_1_ID,
			TIME_SUB_1_ID,			
			-1,
			-1,
			-1,
			-1
		},
		{
			"Tijd            ",
			NULL
		},
		{
			NULL, saveTimeSettings, setCursorPositionLeft, setCursorPositionRight, increaseTime , decreaseTime
		}
	},
	{
		TIME_SUB_SUB_3_1_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			TIME_SUB_3_ID,
			TIME_SUB_3_ID,
			-1,
			-1,
			-1,
			-1
		},
		{
			"Standby tijd    ",
			NULL
		},
		{
			NULL, saveStandbyTime, NULL, NULL, increaseStandbyTime, decreaseStandbyTime
		}
	},
	{
		DATE_SUB_1_ID,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			SETTINGS_SUB_5_ID,
			SETTINGS_SUB_5_ID,
			-1,
			-1,
			-1,
			-1
		},
		{
			"Datum           ",
			NULL
		},
		{
			NULL, saveDateSettings, setCursorPositionLeft, setCursorPositionRight, increaseDate, decreaseDate
		}
	},
		{
			WEB_SUB_1_ID,
			{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
				SETTINGS_SUB_6_ID,
				-1,
				-1,
				-1,
				-1,
				-1
			},
			{
				"Webinterface    ",
				"OK to connect   "
			},
			{
				NULL, webinterfaceMenu, NULL, NULL, NULL, NULL
			}
		},
	{
		VOLUME_UP,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			-1,
			-1,
			-1,
			-1,
			VOLUME_UP,
			VOLUME_DOWN
		},
		{
			"Volume            ",
			NULL
		},
		{
			NULL, NULL, NULL, NULL, volumeUp, volumeDown
		}
	},
	{
		VOLUME_DOWN,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			-1,
			-1,
			-1,
			-1,
			-1,
			-1
		},
		{
			"Volume            ",
			NULL
		},
		{
			NULL, NULL, NULL, NULL, volumeUp, volumeDown
		}
	},
	{
		RSS_FEED,
		{ /* New item id (Esc, Ok, Left, Right, Up, Down) */
			MENU_MAIN_1_ID,
			-1,
			-1,
			-1,
			VOLUME_UP,
			VOLUME_DOWN
		},
		{
			"RSS feed   HH:MM",
			"No Internet     "
		},
		{
			NULL, NULL, NULL, NULL, volumeUp, volumeDown
		}
	}
};

static USERDATA_STRUCT radioStreams[5] = {
	/* Jazz */
	{
		"144.217.84.8",
		8018,
		1010
	},
	/* Classic rock */
	{
		"91.221.151.240",
		8047,
		1010
	},
	/* Aardschock */
	{
		"178.18.137.248",
		80,
		1010
	},
	/* Slam.fm */
	{
		"212.237.51.88",
		80,
		1010
	},
	/* Qmusic */
	{
		"178.85.138.43",
		8001,
		1010
	}
};

static USERDATA_STRUCT currentStream;

//this function switches between radio stations
void changeRadioStation(void)
{	
	switch(currentMenuId)
	{
		case RADIO_SUB_1_ID:
			currentStream = radioStreams[0];
			break;
		case RADIO_SUB_2_ID:
			currentStream = radioStreams[1];
			break;
		case RADIO_SUB_3_ID:
			currentStream = radioStreams[2];
			break;
		case RADIO_SUB_4_ID:
			currentStream = radioStreams[3];
			break;
		case RADIO_SUB_5_ID:
			currentStream = radioStreams[4];
			break;
		default:
			currentStream = radioStreams[0];
			break;
	}
}

void webinterfaceMenu(void)
{
	LcdWriteStringLine2("Ophalen data....");
	int status = connectToWebInterface();
	switch(status)
	{
		case -1:
		LcdWriteStringLine2("Server offline  ");
		break;
		case 1:
		LcdWriteStringLine2("Data opgehaald! ");
		break;
	}
	
}

void setAlarmTimeIPR(int hoursNew, int minutesFirst, int minutesSecond)
{
	hours = hoursNew;
	minutesOne = minutesFirst;
	minutesTwo = minutesSecond;
	saveAlarmTime();
}

//this function start a radiostream for the currently selected stream
void startRadioStream(void)
{
	if(VsGetStatus() == VS_STATUS_RUNNING)
	{
		stopStream();
		printf("stopped the stream by changing the station");
		return;
	}

	printf("attempting to start a new stream");
	connectToStream(currentStream);
	playStream();
}

//this function stops the currently playing radiostream
void stopRadioStream(void)
{
	stopStream();
}

//this function writes the text of a menu_item onto the LCD module
void WriteLinesToLcd(char *lines[])
{
	if(lines[0] != NULL)
	{
		// Make a local copy of the top line
		char topline[20];	
		strcpy(topline, lines[0]);
		
	
		// Get current time and date
		getTimeString(menuCurTime, 0);
		getDateString(menuCurDate, 0);
	
		// Replace all occurances of "HH:MM" with time and "DD-MM-YY" with date
		strcpy(topline, replace_str(topline, "HH:MM", menuCurTime));
		strcpy(topline, replace_str(topline, "DD-MM-YY", menuCurDate));
		
	
		// Write the chars to the display	
		LcdWriteStringLine1(topline);
	}
	if(lines[1] != NULL)
	{
		if(currentMenuId == RSS_FEED)
		{
			LogMsg_P(LOG_ERR, PSTR("in rss"));
			
			char botline[20];
			
			// Shorten string to fit on the display
			if(strlen(rss_title) > 16)
			{
				uint8_t counter = 0;
				uint8_t max_length = 16;
			
				while(counter < max_length)
				{
					botline[counter] = rss_title[counter];
					counter++;
				}
			}
			else
			{
				strcpy(botline, rss_title);
			}
			
			LcdWriteStringLine2(botline);
		}
		else
		{
			// Write the chars to the bottom line of the display
			LcdWriteStringLine2(lines[1]);
		}
	}
}

int firstDisplay = 0;
//this function checks which key is pressed and determines what to do next
void HandleMenu(int key)
{
	int nextId = -1;
	switch(key)
	{
		case KEY_ESC:
			nextId = menu_items[currentMenuItem].newId[MENU_KEY_ESC];
			if(menu_items[currentMenuItem].keyAction[MENU_KEY_ESC] != NULL)
				(*menu_items[currentMenuItem].keyAction[MENU_KEY_ESC])();
			break;
		case KEY_OK:
			nextId = menu_items[currentMenuItem].newId[MENU_KEY_OK];
			if(menu_items[currentMenuItem].keyAction[MENU_KEY_OK] != NULL)
				(*menu_items[currentMenuItem].keyAction[MENU_KEY_OK])();
			break;
		case KEY_LEFT:
			nextId = menu_items[currentMenuItem].newId[MENU_KEY_LEFT];
			if(menu_items[currentMenuItem].keyAction[MENU_KEY_LEFT] != NULL)
				(*menu_items[currentMenuItem].keyAction[MENU_KEY_LEFT])();
			break;
		case KEY_RIGHT:
			nextId = menu_items[currentMenuItem].newId[MENU_KEY_RIGHT];
			if(menu_items[currentMenuItem].keyAction[MENU_KEY_RIGHT] != NULL)
				(*menu_items[currentMenuItem].keyAction[MENU_KEY_RIGHT])();
			break;
		case KEY_UP:
			nextId = menu_items[currentMenuItem].newId[MENU_KEY_UP];
			if(menu_items[currentMenuItem].keyAction[MENU_KEY_UP] != NULL)
				(*menu_items[currentMenuItem].keyAction[MENU_KEY_UP])();
			break;
		case KEY_DOWN:
			nextId = menu_items[currentMenuItem].newId[MENU_KEY_DOWN];
			if(menu_items[currentMenuItem].keyAction[MENU_KEY_DOWN] != NULL)
				(*menu_items[currentMenuItem].keyAction[MENU_KEY_DOWN])();
			break;
		default:
			break;
	}
	
	if(nextId == VOLUME_UP || nextId == VOLUME_DOWN)
	{
		if(currentMenuId != VOLUME_UP || currentMenuId != VOLUME_DOWN)
		{
			previousMenuId = currentMenuId;
			currentMenuItem = 0;
			while(menu_items[currentMenuItem].id != currentMenuId)
			{
				currentMenuItem++;
			}
			previousMenuItem = currentMenuItem;
		}
	} 
	
	if(currentMenuId != nextId && nextId != -1)
	{
		firstDisplay = 0;
		currentMenuId = nextId;
		currentMenuItem = 0;
	
		while(menu_items[currentMenuItem].id != currentMenuId)
		{
			currentMenuItem++;
		}
	}
	
	if(simonSaysActive != 1)
	{
		if(firstDisplay == 0)
		{
			WriteLinesToLcd(menu_items[currentMenuItem].text);
			firstDisplay = 1;
		}
		else if(currentMenuId != DATE_SUB_1_ID &&
		currentMenuId != TIME_SUB_SUB_1_1_ID &&
		currentMenuId != TIME_SUB_SUB_2_1_ID &&
		currentMenuId != TIME_SUB_SUB_3_1_ID &&
		currentMenuId != VOLUME_UP &&
		currentMenuId != VOLUME_DOWN &&
		currentMenuId != ALARM_SUB_SUB_1_1_ID)
		{
			WriteLinesToLcd(menu_items[currentMenuItem].text);
		}
	}
	
}

//this function start the Simon Say's application on the IPAC
void startSimonSays(void)
{
	simonSaysActive = 1;
	startSimonSaysThread();
}

//this function stops the Simon Say's application on the IPAC
void stopSimonSays(void)
{
	simonSaysActive = 0;
	LogMsg_P(LOG_INFO, PSTR("Stopped Simon Says Thread"));
}

void SetDayAlarm(void)
{
	int dayAlarmTime = hours*60 + minutesOne*10 + minutesTwo;
	int day = currentDayId - 2120;
	
	dailyAlarms[day] = dayAlarmTime;	
	addAlarm(day, dayAlarmTime);
}

void SetCurrentDayID(void)
{
	currentDayId = currentMenuId;
    setCurrentTimeToDisplay();
}

//this function saves the internal time settings
void saveTimeSettings(void)
{
	int totalMinutes = minutesOne*10 + minutesTwo;
	setTime(0, totalMinutes, hours);
}

//this function saves the internal date settings
void saveDateSettings(void)
{
	if(weekDay == 6)
	{
		weekDay = 0;
	}
    else
	{
		weekDay = weekDay + 1;
	}
	LogMsg_P(LOG_INFO, PSTR("weekday %i"), weekDay);
	setDate(day, month, year, weekDay);
}

void setNewDate(int dayNew, int monthNew, int yearNew)
{
	day = dayNew;
	month = monthNew;
	year = yearNew;
	saveDateSettings();
}

//this function saves the internal alarm time settings
void saveAlarmTime(void)
{
	alarmTime = hours*60 + minutesOne*10 + minutesTwo;
	snoozableAlarmTime = alarmTime;
	saveAlarmTimeToChip(alarmTime);
}

//this function saves the internal network settings
void saveMacAddressSettings(void)
{
	int hex,i;
	
	for(i=0; i<6; i++)
	{
		if(i == 0)
		{
			hex = (macHexArray[i] * 16) + macHexArray[i + 1];
		}
		else 
		{
			hex = (macHexArray[(i*2)] * 16) + macHexArray[(i*2)+1];
		}
		
		macAddress[i] = hex;
	}
	saveMacAddress();
}

//this function increases the cursor position to the left by 1 if possible
void setCursorPositionLeft(void)
{
	int minimumPosition = 0;
	switch(currentMenuId)
	{
		case TIME_SUB_SUB_1_1_ID:
			minimumPosition = 1;
			break;
		case ALARM_SUB_SUB_1_1_ID:
			minimumPosition = 1;
			break;
		case DATE_SUB_1_ID:
			minimumPosition = 2;
			break;
		case NETWORK_SUB_1_ID:
			minimumPosition = 11;
			break;
		case ALARM_SUB_SUB_TIME_2_1_1_ID:
			minimumPosition = 1;
			break;
	}

	if(cursorPosition > minimumPosition)
	{
		cursorPosition--;
	}
}

//this function increases the cursor position to the left by 1 if possible
void setCursorPositionRight(void)
{
	int maximumPosition = 0;
	switch(currentMenuId)
	{
		case TIME_SUB_SUB_1_1_ID:
			maximumPosition = 3;
			break;
		case ALARM_SUB_SUB_1_1_ID:
			maximumPosition = 3;
			break;
		case DATE_SUB_1_ID:
			maximumPosition = 2;
			break;
		case NETWORK_SUB_1_ID:
			maximumPosition = 11;
			break;
		case ALARM_SUB_SUB_TIME_2_1_1_ID:
			maximumPosition = 3;
			break;
	}

	if(cursorPosition < maximumPosition)
	{
		cursorPosition++;
	}
}

//this function updates the time settings display
void updateSetTimeDisplay(void)
{
	char timeBuffer[6];
	sprintf(timeBuffer, "%02d:%d%d", hours, minutesOne, minutesTwo);
	LcdWriteStringLine2(timeBuffer);
}

//this function updates the date settings display
void updateSetDateDisplay(void)
{
	char dateBuffer[11];
	sprintf(dateBuffer, "%02d-%02d-%d", day, month, year);
	LcdWriteStringLine2(dateBuffer);
}

//this function updates the mac address settings display
void updateSetMacAddressDisplay(void)
{
	char macBuffer1[17];
	char macBuffer2[17];
	sprintf(macBuffer1, "<   %X%X-%X%X-%X%X   >", macHexArray[0],macHexArray[1],macHexArray[2],macHexArray[3],macHexArray[4],macHexArray[5]);
	sprintf(macBuffer2, "<   %X%X-%X%X-%X%X   >", macHexArray[6],macHexArray[7],macHexArray[8],macHexArray[9],macHexArray[10],macHexArray[11]);	
	
	LcdWriteStringLine1(macBuffer1);
	LcdWriteStringLine2(macBuffer2);
}

//this function increases the mac address value thats being configured by 1 in the network settings
void increaseMacAddress(void)
{
	if(macHexArray[cursorPosition] < 15)
	{
		macHexArray[cursorPosition] += 1;
	}
	updateSetMacAddressDisplay();
}

//this function decreases the mac address value thats being configured by 1 in the network settings
void decreaseMacAddress(void)
{
	if(macHexArray[cursorPosition] > 0)
	{
		macHexArray[cursorPosition] -= 1;
	}
	updateSetMacAddressDisplay();
}

//this function increases the time thats being configured by 1 in the time settings
void increaseTime(void)
{
	switch(cursorPosition)
	{
		case 1:
			if(hours < 23)
			{
				hours += 1;
			}
			break;
		case 2:
			if(minutesOne < 5)
			{
				minutesOne += 1;
				if(minutesOne == 6)
				{
					minutesTwo = 0;
				}
			}
			break;
		case 3:
			if(minutesTwo < 9)
			{
			
				minutesTwo += 1;
			}
			break;
	}
	updateSetTimeDisplay();
}

//this function decreases the time thats being configured by 1 in the time settings
void decreaseTime(void)
{
	switch(cursorPosition)
	{
		case 1:
			if(hours >= 1)
			{
				hours -= 1;
			}
			break;
		case 2:
			if(minutesOne >= 1)
			{
				minutesOne -= 1;
			}
			break;
		case 3:
			if(minutesTwo >= 1)
			{
				minutesTwo -= 1;
			}
			break;
	}
	updateSetTimeDisplay();
}

//this function increases the date thats being configured by 1 in the date settings
void increaseDate(void)
{
	int maxDaysFeb = 28;
	if(year%4 == 0)
	{
		maxDaysFeb = 29;
	}
	switch(cursorPosition)
	{
		case 0:
			if(month == 2)
			{
				if(day < maxDaysFeb)
				{
					day++;
				}
			}
			else
			{
				if(day < daysPerMonth[month-1])
				{
					day++;
				}
			}
			break;
		case 1:
			if(month < 12)
			{
				month++;
				if(day > daysPerMonth[month-1])
				{
					day = daysPerMonth[month-1];
				}
			}
			break;
		case 2:
			year++;
			break;
	}
	updateSetDateDisplay();
}

//this function decreases the date thats being configured by 1 in the date settings
void decreaseDate(void)
{
	switch(cursorPosition)
	{
		case 0:
			if(day > 1)
			{
				day--;
			}
			break;
		case 1:
			if(month > 1)
			{
				month--;
				if(day > daysPerMonth[month-1])
				{
					day = daysPerMonth[month-1];
				}
			}
			break;
		case 2:
			year--;
			break;
	}
	updateSetDateDisplay();
}

//this function sets the current time to the LCD module when the user navigates to the time settings
void setCurrentTimeToDisplay(void)
{
	int currentMinutes;
	int currentHours;
	char *currentTimeString = "";
	getTimeString(currentTimeString, 0);
	LcdWriteStringLine2("                ");
	LcdWriteStringLine2("00:00           ");
	LcdWriteStringLine2(currentTimeString);
	getTime(0, &currentMinutes, &currentHours);
	hours = currentHours;
	minutesTwo = ((int)currentMinutes % 10);
	currentMinutes = currentMinutes - minutesTwo;
	minutesOne = ((int)currentMinutes/10);

	cursorPosition = 1;
}

//this function sets the current date to the LCD module when the user navigates to the date settings
void setCurrentDateToDisplay(void)
{
	int currentDay;
	int currentMonth;
	int currentYear;
	char *currentDateString = "";
	getDateString(currentDateString, 1);
	LcdWriteStringLine2("                ");
	LcdWriteStringLine2("01-01-2000      ");
	LcdWriteStringLine2(currentDateString);
	getDate(&currentDay, &currentMonth, &currentYear);
	day = currentDay;
	month = currentMonth;
	year = currentYear;

	cursorPosition = 0;
}

//this function sets the current mac address to the LCD module when the user navigates to the network settings
void setCurrentMacAddressToDisplay(void)
{
	int i;
	for(i = 0; i < 6; i++)
	{
		macHexArray[i*2+1] = (macAddress[i] % 16);
		macHexArray[i*2] = (macAddress[i] - macHexArray[i*2+1])/16;
	}
	updateSetMacAddressDisplay();	
	cursorPosition = 0;
}

//this function sets the current alarm time to the LCD module when the user navigates to the alarm settings
void setCurrentAlarmTimeToDisplay(void)
{
	int totalMinutes = alarmTime % 60;
	hours = (alarmTime - totalMinutes)/60;
	minutesTwo = totalMinutes % 10;
	minutesOne = (totalMinutes - minutesTwo)/10;
	char *currentAlarmString = "";
	sprintf(currentAlarmString, "%02d:%d%d           ", hours, minutesOne, minutesTwo);
	LcdWriteStringLine2(currentAlarmString);

	cursorPosition = 1;
}

//this function turns up the volume of the device
void volumeUp(void)
{
	volume = VsGetVolume();
	if(volume > 0 && volume <= 3855)
	{
		VsSetVolume(volume - 1, volume - 1);
	}
	LcdWriteVolume(15 - volume/257);
	volumeChanged = 1;
}

void setVolume(int data)
{
	volume = (data * 257);
	VsSetVolume(volume -1, volume -1);
	
}

//this function turns down the volume of the device
void volumeDown(void)
{
	volume = VsGetVolume();
	if(volume >= 0 && volume < 3855)
	{
		VsSetVolume(volume + 1, volume + 1);
	}
	LcdWriteVolume(15 - volume/257);
	volumeChanged = 1;
}

//this function sets the standby time to LCD module when the user navigates to the standby time settings
void setStandbyTimeToDisplay(void)
{
	LcdWriteStringLine2("                ");
	LcdWriteStringLine2(standbyTimes[standbyTime]);
}

//this function increases the standby time by one increment
void increaseStandbyTime(void)
{
	if(standbyTime < 11)
	{
		standbyTime++;
	}
	LcdWriteStringLine2(standbyTimes[standbyTime]);
}

//this function decreases the standby time by one increment
void decreaseStandbyTime(void)
{
	if(standbyTime > 0)
	{
		standbyTime--;
	}
	LcdWriteStringLine2(standbyTimes[standbyTime]);
}

void setStandbyTime(int data)
{
	standbyTime = data;
	saveStandbyTime();
}

//this function saves the standbyTime to the falsh memory of the IPAC
void saveStandbyTime(void)
{
	saveStandbyTimeToChip(standbyTime);
}

//this function saves the mac address to the falsh memory of the IPAC
void saveMacAddress(void)
{
	int macBuffer[] = {macAddress[0], macAddress[1], macAddress[2], macAddress[3], macAddress[4], macAddress[5]};
	int i;
	for(i = 0; i < 6; i++)
	{
		saveMacAddressToChip(macBuffer[i], 10+i);
	}
}

//this function turns off the power of the device
void power(void)
{
	if(powerState == 0)
	{
		LcdBackLight(LCD_BACKLIGHT_ON);
		currentMenuId = MENU_STANDBY;
		currentMenuItem = 0;
		WriteLinesToLcd(menu_items[MENU_STANDBY].text);
		powerState = 1;
	}
	else if(powerState == 1)
	{
		LcdBackLight(LCD_BACKLIGHT_OFF);
		LcdWriteStringLine1("                ");
		LcdWriteStringLine2("                ");
		powerState = 0;
	} 
}

//this function handles the duration of the display of the volume screen
void checkVolumeDelay(void)
{
	if(volumeChanged)
	{
		int volumeDelay;
		getTime(&volumeDelay, &min, &hour);
		if(sec < 10 && volumeDelay >= 10)
		{
			sec = sec+60;
		}
		else if(sec >= 10 && volumeDelay < 10)
		{
			volumeDelay = volumeDelay+60;
		}
		
		if(volumeDelay >= sec+2)
		{
			WriteLinesToLcd(menu_items[previousMenuItem].text);
			currentMenuId = previousMenuId;
			currentMenuItem = previousMenuItem;
			volumeChanged = 0;
		}
		
		if(sec >= 60)
		{
			sec = sec-60;
		}
		if(volumeDelay >= 60)
		{
			volumeDelay = volumeDelay-60;
		}
	}
}

//this function handles the duration of the standby mode of the device
void checkStandbyMode()
{
	if(alarmOn == 0)
	{
		int standbyCounter;
		getTime(&standbyCounter, &min, &hour);
		if(sec < 10 && standbyCounter >= 10)
		{
			sec = sec+60;
		}
		else if(sec >= 10 && standbyCounter < 10)
		{
			standbyCounter = standbyCounter+60;
		}
	
		if(standbyCounter >= sec+((standbyTime+1)*5))
		{
			LcdBackLight(LCD_BACKLIGHT_OFF);
			backlight = LCD_BACKLIGHT_OFF;
		}
	
		if(sec >= 60)
		{
			sec = sec-60;
		}
		if(standbyCounter >= 60)
		{
			standbyCounter = standbyCounter-60;
		}
	}
	else if (alarmOn == 1)
	{
		LcdBackLight(LCD_BACKLIGHT_ON);
		backlight = LCD_BACKLIGHT_ON;
	}
}

//this function controls the navigation through the menustructure
THREAD( MenuThread, arg)
{	
	currentStream = radioStreams[0];
	
	readStandbyTime(&standbyTime);					//read standby time from flash
	if(standbyTime > 11 || standbyTime < 0 )		// check for standbyTime, if standbyTime is not a acceptable value standby time = 1
	{
		standbyTime = 1;
	}
	printf("In the thread: %i", standbyTime);

	int i;
	for(i = 0; i < 6; i++)
	{
		readMacAddress(&macAddress[i], 10+i);		//read MacAddress from flash
		if(macAddress[i] > 255)
		{
			macAddress[i] = 0;
		}
	}	
	for(i = 0; i < 7; i++)
	{
		readDailyAlarm(&dailyAlarms[i], i);   // read daily alarm from flash
		LogMsg_P(LOG_INFO, PSTR("read alarm %i"), dailyAlarms[i]);
	}
	readAlarmTime(&alarmTime);						//read alarm time from flash

	int remoteKey;
	int key;
	int prevKey = NO_KEY_INPUT;
	int prevRemoteKey = NO_KEY_INPUT;
	currentMenuId = menu_items[currentMenuItem].id;
	WriteLinesToLcd(menu_items[0].text);
		
 	key = GetKeyFound();
	remoteKey = RcGetKeyboardKey();              //read remote input
	
	int prevMin;
	int x;
	
	getTime(&sec, &min, &hour);
	getCurrentDayOfWeek(&weekDay);
	if(weekDay == -1)
	{
		weekDay = 1; // Weekday is Monday
	}
	LogMsg_P(LOG_INFO, PSTR("weekday: %i"), weekDay);

	snoozableAlarmTime = alarmTime;
	inetStatus = testInetConnection();
	writeInternetStatusToDisplay(inetStatus);
	rss_title = getLatestRssTitle();
	//strcpy(rss_title, getLatestRssTitle());
	
	snoozableAlarmTime = alarmTime;
	while(1)
	{
		if(simonSaysActive != 1)
		{
			getDateString(menuCurDate, 0);
			getTimeString(menuCurTime, 0);
			//LogMsg_P(LOG_INFO, PSTR("set day alarm %i"), dailyAlarms[weekDay]);
			//LogMsg_P(LOG_INFO, PSTR("day time %i"), (hour * 60) + min);
			prevMin = min;
			getTime(&x, &min, &hour);
			if((hour * 60 + min) >= snoozableAlarmTime && (hour * 60 + min) < snoozableAlarmTime + 3)
			{
				VsBeep(100,100);
				alarmOn = 1;
			 }
			 else if ((hour * 60 + min) >= dailyAlarms[weekDay] && (hour * 60 + min) < dailyAlarms[weekDay] + 3)
			 {
				alarmOn = 1;
				VsPlayerInit();
				VsBeep(5, 100);
			 }
			 else
			 {
				alarmOn = 0;
				if((hour * 60 + min) == snoozableAlarmTime + 3 || dailyAlarms[weekDay] == snoozableAlarmTime + 3)
				{
					VsBeep(100,100);
					alarmOn = 1;
				}
				else
				{
					alarmOn = 0;
					if((hour * 60 + min) == snoozableAlarmTime + 3)
					{
						snoozableAlarmTime = alarmTime;
					}
				}
			 }
			
				checkStandbyMode();
				checkVolumeDelay();
				if(key != prevKey || remoteKey != prevRemoteKey)
				{
					getTime(&sec, &min, &hour);
					if(backlight == LCD_BACKLIGHT_OFF)
					{
						LcdBackLight(LCD_BACKLIGHT_ON);
						backlight = LCD_BACKLIGHT_ON;
					}
					else if(key == KEY_ALT || remoteKey == KEY_ALT)
					{
						if(alarmOn)
						{
							snoozableAlarmTime = hour*60 + min + 2;
						}
					}
					else if(key == KEY_POWER || remoteKey == KEY_POWER)
					{
						power();
					}
					else if(powerState != 0)
					{
						if(key != prevKey)
						{
							HandleMenu(key);
						}
						else if(remoteKey != prevRemoteKey)
						{
							HandleMenu(remoteKey);
						}
					}
				}
			
				if(min != prevMin)
				{
					HandleMenu(NO_KEY_INPUT);
				
					if(currentMenuId == MENU_STANDBY && powerState == 1)
					{
						inetStatus = testInetConnection();
					}
					
					LogMsg_P(LOG_INFO, PSTR("Getting latest RSS title..."));
					rss_title = getLatestRssTitle();	
					//strcpy(rss_title, getLatestRssTitle());
				}
			
				if(currentMenuId == MENU_STANDBY && powerState == 1)
				{
					writeInternetStatusToDisplay(inetStatus);
				}
			
				prevKey = key;
				prevRemoteKey = remoteKey;
			
				key = GetKeyFound();
				remoteKey = RcGetKeyboardKey();		//read remote input
			
		}
		NutSleep(1);
	}
}

//this function starts the menu thread
void startMenuThread()
{
	NutThreadCreate("MenuThread", MenuThread, NULL, 512);
}
