/*! \mainpage SIR firmware documentation
 *
 *  \section intro Introduction
 *  A collection of HTML-files has been generated using the documentation in the sourcefiles to
 *  allow the developer to browse through the technical documentation of this project.
 *  \par
 *  \note these HTML files are automatically generated (using DoxyGen) and all modifications in the
 *  documentation should be done via the sourcefiles.
 */

/*! \file
 *  COPYRIGHT (C) STREAMIT BV 2010
 *  \date 19 december 2003
 */
#define LOG_MODULE  LOG_MAIN_MODULE
//#define USE_JTAG 1

/*--------------------------------------------------------------------------*/
/*  Include files                                                           */
/*--------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>

#include <arpa/inet.h>
#include <pro/sntp.h> 

#include "Application/menu.h"

#include "Control/util.h"
#include "Control/system.h"
#include "Control/portio.h"
#include "Control/display.h"
#include "Control/shoutcast.h"
#include "Control/watchdog.h"
#include "Control/log.h"
#include "Control/timeutils.h"
#include "Control/internetutils.h"
#include "Control/userdata.h"
#include "Control/rss.h"

#include "Control/webinterface.h"

#include "Hardware/remcon.h"
#include "Hardware/keyboard.h"
#include "Hardware/led.h"
#include "Hardware/uart0driver.h"
#include "Hardware/mmc.h"
#include "Hardware/flash.h"
#include "Hardware/spidrv.h"
#include "Hardware/vs10xx.h"
#include "Hardware/rtc.h"

void showUserData(USERDATA_STRUCT *streams, int len);

/*-------------------------------------------------------------------------*/
/* global variable definitions                                             */
/*-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/* local variable definitions                                              */
/*-------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------*/
/* local routines (prototyping)                                            */
/*-------------------------------------------------------------------------*/
static void SysMainBeatInterrupt(void*);
static void SysControlMainBeat(u_char);
/*-------------------------------------------------------------------------*/
/* Stack check variables placed in .noinit section                         */
/*-------------------------------------------------------------------------*/

/*!
 * \addtogroup System
 */

/*@{*/


/*-------------------------------------------------------------------------*/
/*                         start of code                                   */
/*-------------------------------------------------------------------------*/


/* ����������������������������������������������������������������������� */
/*!
 * \brief ISR MainBeat Timer Interrupt (Timer 2 for Mega128, Timer 0 for Mega256).
 *
 * This routine is automatically called during system
 * initialization.
 *
 * resolution of this Timer ISR is 4,448 msecs
 *
 * \param *p not used (might be used to pass parms from the ISR)
 */
/* ����������������������������������������������������������������������� */
static void SysMainBeatInterrupt(void *p)
{

    /*
     *  scan for valid keys AND check if a MMCard is inserted or removed
     */
    KbScan();
    CardCheckCard();
}


/* ����������������������������������������������������������������������� */
/*!
 * \brief Initialise Digital IO
 *  init inputs to '0', outputs to '1' (DDRxn='0' or '1')
 *
 *  Pull-ups are enabled when the pin is set to input (DDRxn='0') and then a '1'
 *  is written to the pin (PORTxn='1')
 */
/* ����������������������������������������������������������������������� */
void SysInitIO(void)
{
    /*
     *  Port B:     VS1011, MMC CS/WP, SPI
     *  output:     all, except b3 (SPI Master In)
     *  input:      SPI Master In
     *  pull-up:    none
     */
    outp(0xF7, DDRB);

    /*
     *  Port C:     Address bus
     */

    /*
     *  Port D:     LCD_data, Keypad Col 2 & Col 3, SDA & SCL (TWI)
     *  output:     Keyboard colums 2 & 3
     *  input:      LCD_data, SDA, SCL (TWI)
     *  pull-up:    LCD_data, SDA & SCL
     */
    outp(0x0C, DDRD);
    outp((inp(PORTD) & 0x0C) | 0xF3, PORTD);

    /*
     *  Port E:     CS Flash, VS1011 (DREQ), RTL8019, LCD BL/Enable, IR, USB Rx/Tx
     *  output:     CS Flash, LCD BL/Enable, USB Tx
     *  input:      VS1011 (DREQ), RTL8019, IR
     *  pull-up:    USB Rx
     */
    outp(0x8E, DDRE);
    outp((inp(PORTE) & 0x8E) | 0x01, PORTE);

    /*
     *  Port F:     Keyboard_Rows, JTAG-connector, LED, LCD RS/RW, MCC-detect
     *  output:     LCD RS/RW, LED
     *  input:      Keyboard_Rows, MCC-detect
     *  pull-up:    Keyboard_Rows, MCC-detect
     *  note:       Key row 0 & 1 are shared with JTAG TCK/TMS. Cannot be used concurrent
     */
#ifndef USE_JTAG
    sbi(JTAG_REG, JTD); // disable JTAG interface to be able to use all key-rows
    sbi(JTAG_REG, JTD); // do it 2 times - according to requirements ATMEGA128 datasheet: see page 256

	
#endif //USE_JTAG

	cbi(OCDR, IDRD);
	cbi(OCDR, IDRD);


    outp(0x0E, DDRF);
    outp((inp(PORTF) & 0x0E) | 0xF1, PORTF);

    /*
     *  Port G:     Keyboard_cols, Bus_control
     *  output:     Keyboard_cols
     *  input:      Bus Control (internal control)
     *  pull-up:    none
     */
    outp(0x18, DDRG);
}

/* ����������������������������������������������������������������������� */
/*!
 * \brief Starts or stops the 4.44 msec mainbeat of the system
 * \param OnOff indicates if the mainbeat needs to start or to stop
 */
/* ����������������������������������������������������������������������� */
static void SysControlMainBeat(u_char OnOff)
{
    int nError = 0;

    if (OnOff==ON)
    {
        nError = NutRegisterIrqHandler(&OVERFLOW_SIGNAL, SysMainBeatInterrupt, NULL);
        if (nError == 0)
        {
            init_8_bit_timer();
        }
    }
    else
    {
        // disable overflow interrupt
        disable_8_bit_timer_ovfl_int();
    }
}

/************************************************************************/
/* Show the current time on the display                                                                     */
/************************************************************************/
void printCurrentTime(void)
{
	char temp[10];
	getTimeString(temp, 1);
	LcdWriteString(temp);
}

/************************************************************************/
/* print the current date on the display                                                                     */
/************************************************************************/
void printCurrentDate(void)
{
	char temp[12];
	getDateString(temp, 1);
	LcdWriteString(temp);
}

/************************************************************************/
/* Thread to update time on lcd display                                                                     */
/************************************************************************/
THREAD(ShowTimeThread, arg)
{		
	while(1)
	{			
		int sec, min, hour;
		getTime(&sec, &min, &hour);
		
		//LogMsg_P(LOG_INFO, PSTR("Current RTC Time: [%02d:%02d:%02d]"), hour, min, sec);

		NutSleep(1000);
	}
}

/* ����������������������������������������������������������������������� */
/*!
 * \brief Main entry of the SIR firmware
 *
 */
/* ����������������������������������������������������������������������� */
int main(void)
{
    //  First disable the watchdog
    WatchDogDisable();

    NutDelay(100);

    SysInitIO();
	SPIinit();
	LedInit();
	LcdLowLevelInit();
    Uart0DriverInit();
    Uart0DriverStart();
	LogInit();
    CardInit();
    X12Init();
	RcInit();												//inintialize remote controller
	KbInit();
	At45dbInit();											// initialize flash
	VsPlayerInit();
		
    SysControlMainBeat(ON);             // enable 4.4 msecs hartbeat interrupt
	
	LogMsg_P(LOG_INFO, PSTR("Hello World"));

	LcdBackLight(LCD_BACKLIGHT_ON);
	NutThreadCreate("ShowTime", ShowTimeThread, NULL, 512);
	
	/* Init network adapter */
	if( OK != initInet() )
	{
		LogMsg_P(LOG_ERR, PSTR("initInet() = NOK, NO network!"));
	}
	
	/* Get time from NTP server and set it in the RTC */
	tm ntptime;
	if(getNTPTime(&ntptime) == 1)
	{
		LogMsg_P(LOG_INFO, PSTR("NTP time is: [%02d:%02d:%02d]"), ntptime.tm_hour, ntptime.tm_min, ntptime.tm_sec);
		LogMsg_P(LOG_INFO, PSTR("NTP date is: [%02d-%02d-%02d]"), ntptime.tm_mday, ntptime.tm_mon, ntptime.tm_year);
		setTime(ntptime.tm_sec, ntptime.tm_min, ntptime.tm_hour);
		setDate(ntptime.tm_mday, ntptime.tm_mon + 1, ntptime.tm_year + 1900, ntptime.tm_wday);
	}

	startMenuThread();
		
	NutSleep(1000);

    /*
     * Increase our priority so we can feed the watchdog.
     */
    NutThreadSetPriority(1);

	/* Enable global interrupts */
	sei();

	for (;;)
    {
        WatchDogRestart();
		NutSleep(1000);
    }

    return(0);      // never reached, but 'main()' returns a non-void, so.....
}

