/*! \file
 * remcon.c contains all interface- and low-level routines that
 * perform handling of the infrared bitstream
 * [COPYRIGHT]  Copyright (C) STREAMIT BV
 *  \version 1.0
 *  \date 26 september 2003
 *	\[UPDATED] 21-03-2019
 *	\[OWNER]	Thimo
 *	\
 */

#define LOG_MODULE  LOG_REMCON_MODULE

#define LENGTH_LEAD_MIN				2900
#define LENGTH_LEAD_MAX				3300
#define LENGTH_BIT_HIGH_MIN			400
#define LENGTH_BIT_HIGH_MAX			600
#define LENGTH_BIT_LOW_MIN			200
#define LENGTH_BIT_LOW_MAX			300

#define SKIP_ADDRESS_CODE			16
#define END_OF_COMMAND_CODE			25
#define LAST_BIT					32
#define COMMAND_CODE_RESET			-1
#define START_OF_DATA				24


#include <stdlib.h>
#include <fs/typedefs.h>
#include <sys/heap.h>

#include <sys/event.h>
#include <sys/atom.h>
#include <sys/types.h>
#include <dev/irqreg.h>

#include "Control/system.h"
#include "Control/portio.h"
#include "Hardware/remcon.h"
#include "Control/display.h"
#include "Hardware/keyboard.h"
#include "Hardware/led.h"
#include "Control/log.h"


//NEC protocol remote code: 
//SET + TV + 057

/*-------------------------------------------------------------------------*/
/* local variable definitions                                              */
/*-------------------------------------------------------------------------*/
static HANDLE  hRCEvent;
int irStarted = 0;
int currentBit = 1;
int currentCode = 0;
int finalCode = 0;
uint16_t timePrevInterrupt = 0;

/*-------------------------------------------------------------------------*/
/* local routines (prototyping)                                            */
/*-------------------------------------------------------------------------*/
static void RcInterrupt(void *);
static void RcClearEvent(HANDLE*);
void RcRceivedBit(int);
int RcKeyToKbKey(int);
int RcGetKeyboardKey(void);

/*!
 * \addtogroup RemoteControl
 */

/*@{*/

/*-------------------------------------------------------------------------*/
/*                         start of code                                   */
/*-------------------------------------------------------------------------*/

/*!
 * \brief ISR Remote Control Interrupt (ISR called by Nut/OS)
 * param *p will not be used
 */
static void RcInterrupt(void *p)
{
    uint16_t timer = TCNT1;
    uint16_t difference = timer - timePrevInterrupt;  //Time between last interrupt and now.
    if(!irStarted) {
        if(difference > LENGTH_LEAD_MIN && difference < LENGTH_LEAD_MAX) {
            irStarted = 1; // Leader received, listening to IR-code now.
        }
    } else {
		LedControl(LED_TOGGLE);
        if(difference > LENGTH_BIT_HIGH_MIN && difference < LENGTH_BIT_HIGH_MAX) {
            RcRceivedBit(1); // A 1 bit is received
        } else if(difference > LENGTH_BIT_LOW_MIN && difference < LENGTH_BIT_LOW_MAX) {	
            RcRceivedBit(0); // A 0 bit is received
        } 
		LedControl(LED_TOGGLE);
    }
    timePrevInterrupt = timer; // Set last time interrupted to now.
}

/*
* Analyze a bit and paste it to the variable that connects all bits
* If a full signal is received, paste the code in the finalCode variable
*/

void RcRceivedBit(int ReceivedBit) {
    if(currentBit > SKIP_ADDRESS_CODE && currentBit < END_OF_COMMAND_CODE) { //If this bit is a command (data) bit add it to currentCode
        if(ReceivedBit) {
            currentCode += (1 << (START_OF_DATA - currentBit)); // paste the bits behind each other from the Command bits
        }
        LedControl(LED_TOGGLE);			
    } else if(currentBit >= LAST_BIT) { // When all bits are analyzed, paste the currentCode into finalCode and reset for the next signal
        irStarted = 0;
        currentBit = 0;
        finalCode = currentCode;		
        currentCode = 0;				//Reset currentCode for a following receive
        LedControl(LED_OFF);
    }
    currentBit++;
	LedControl(LED_TOGGLE);
}

/*
* Convert the remote code into a keyboard code and return that code
*/
int RcGetKeyboardKey(){
	int RcKbCode = RcKeyToKbKey(finalCode);
	finalCode = COMMAND_CODE_RESET;					//reset finalcode to -1 because a remote button (CH+) has a 0 code return
	return RcKbCode;
}

/*
* Translate the received code from the remote to a button on the internetradio
*/
int RcKeyToKbKey(int RcKey) {
	switch(RcKey) {
		case RC_BUTTON_N1 :		return KEY_01;
		case RC_BUTTON_N2 :		return KEY_02;
		case RC_BUTTON_N3 :		return KEY_03;
		case RC_BUTTON_N4 :		return KEY_04;
		case RC_BUTTON_N5 :		return KEY_05;
		
		case RC_BUTTON_UP :		return KEY_UP;
		case RC_BUTTON_RIGHT :	return KEY_RIGHT;
		case RC_BUTTON_LEFT :	return KEY_LEFT;
		case RC_BUTTON_DOWN :	return KEY_DOWN;
		
		case RC_BUTTON_POWER :	return KEY_POWER;
		case RC_BUTTON_ESC :	return KEY_ESC;
		case RC_BUTTON_OK :		return KEY_OK;
		case RC_BUTTON_ALT :	return KEY_ALT;
	}
	return KEY_UNDEFINED;				//can't understand the received keycode
}

/*
* Print the code from a button on the debug display
*/
void printRemoteconInfo() {
	LogMsg_P(LOG_INFO, PSTR("Lastreceived code: %d"), finalCode);
}

/*!
 * \brief Clear the eventbuffer of this module
 *
 * This routine is called during module initialization.
 *
 * \param *pEvent pointer to the event queue
 */
static void RcClearEvent(HANDLE *pEvent)
{
    NutEnterCritical();
    *pEvent = 0;
    NutExitCritical();
}

/*!
 * \brief Initialise the Remote Control module
 *
 * - register the ISR in NutOS
 * - initialise the HW-timer that is used for this module (Timer1)
 * - initialise the external interrupt that inputs the infrared data
 * - flush the remote control buffer
 * - flush the eventqueue for this module
 */
void RcInit()
{
    int nError = 0;

    EICRB &= ~RC_INT_SENS_MASK;    // clear b0, b1 of EICRB

    // Install Remote Control interrupt
    nError = NutRegisterIrqHandler(&sig_INTERRUPT4, RcInterrupt, NULL);
    if (nError == FALSE)
    {
        EICRB |= RC_INT_FALLING_EDGE;
        EIMSK |= 1<<IRQ_INT4;         // enable interrupt
    }

    // Initialise 16-bit Timer (Timer1)
    TCCR1B |= (1<<CS11) | (1<<CS10); // clockdivider = 64
    TIFR   |= 1<<ICF1;

    RcClearEvent(&hRCEvent);
}

/* ---------- end of module ------------------------------------------------ */

/*@}*/
