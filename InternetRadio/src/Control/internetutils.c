/*
 * internetutils.c
 *
 * Created: 28-2-2019 15:39:00
 *  Author: Tom Martens
 */

#include <sys/thread.h>
#include <sys/timer.h>

#include <arpa/inet.h> /* For inet_addr() */
#include <pro/sntp.h> /*  For NTP */
#include <dev/nicrtl.h>
#include <sys/confnet.h>
#include <pro/dhcp.h>
#include <sys/socket.h>

#include "Control/userdata.h"
#include "Control/log.h"


#define LOG_MODULE  LOG_MAIN_MODULE
#define NTP_SERVER	"5.39.184.5"
#define ETH0_BASE	0xC300
#define ETH0_IRQ	5
#define OK			0
#define NOK			-1

 
static char eth0IfName[9] = "eth0";


int t = 0;
time_t ntp_time = 0;
tm *ntp_datetime;
uint32_t timeserver = 0;
 
int initInet(void)
{
	uint8_t mac_addr[6] = {0x00, 0x08, 0x98, 0x30, 0x04, 0x76 };
	
	int result = OK;

	result = NutRegisterDevice(&DEV_ETHER, ETH0_BASE, ETH0_IRQ);
	LogMsg_P(LOG_DEBUG, PSTR("NutRegisterDevice() returned %d"), result);
	if(result)
	{
		LogMsg_P(LOG_ERR, PSTR("Error: >> NutRegisterDevice()%"));
		result = NOK;
	}
	
	if( OK == result )
	{
		LogMsg_P(LOG_INFO, PSTR("Getting IP address (DHCP)"));

		if( NutDhcpIfConfig(eth0IfName, mac_addr, 10000) )
		{
			LogMsg_P(LOG_ERR, PSTR("Error: >> NutDhcpIfConfig()"));
			result = NOK;
		}
	}
	
	LogMsg_P(LOG_INFO, PSTR("Networking setup OK, new settings are:\n") );

	LogMsg_P(LOG_INFO, PSTR("if_name: %s"), confnet.cd_name);
	LogMsg_P(LOG_INFO, PSTR("ip-addr: %s"), inet_ntoa(confnet.cdn_ip_addr) );
	LogMsg_P(LOG_INFO, PSTR("ip-mask: %s"), inet_ntoa(confnet.cdn_ip_mask) );
	LogMsg_P(LOG_INFO, PSTR("gw     : %s"), inet_ntoa(confnet.cdn_gateway) );
	
	//NutSleep(1000);
	
	return result;
} 
 
//Retrieves time from pool.ntp.org
//Returns 1 on success and 0 on failure
int getNTPTime(tm *ntc_time)
{
	LogMsg_P(LOG_INFO, PSTR("Retrieving time from pool.ntp.org..."));
 
	_timezone = -1 * 60 * 60; /* GMT -1 hour */
 
	timeserver = inet_addr(NTP_SERVER); /* [LiHa] IP address may change frequently (it's a pool afterall) */
 
	if (NutSNTPGetTime(&timeserver, &ntp_time) != 0) /* If it gives an error */
	{
		LogMsg_P(LOG_ERR, PSTR("Failed to retrieve time"));
		return 0;
	} 
	else
	{
		ntp_datetime = localtime(&ntp_time);
		*ntc_time = *ntp_datetime;
		return 1;
	}
}




