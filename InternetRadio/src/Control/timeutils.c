/*
 * timeutils.c
 *
 * Created: 19-2-2019 16:27:10
 *  Author: Yannick
 */ 

#include "Control/timeutils.h"

/*!
 * \Gets a formatted string with the time
 *
 * \param *timestr Pointer to settable string for time
 * \param include_seconds 1 = HH:MM:SS, 0 = HH:MM
 */
void getTimeString(char *timestr, int include_seconds)
{
	// Get current time from RTC
	tm gmt;
	X12RtcGetClock(&gmt);
	
	char hour[3];
	char minute[3];
	char seconds[3];

	// Save the parts of the time into chars
	sprintf(hour, "%02d", gmt.tm_hour);
	sprintf(minute, "%02d", gmt.tm_min);
	sprintf(seconds,"%02d", gmt.tm_sec);

	// Create one big string with the full-length string
	strcpy(timestr, hour);
	strcat(timestr, ":");
	strcat(timestr, minute);
	
	if(include_seconds)
	{
		strcat(timestr, ":");
		strcat(timestr, seconds);
	}

}

/*!
 * \Gets a formatted string with the time (DD-MM-YY)/(DD-MM-YYYY)
 *
 * \param *datestr Pointer to settable string for time
 * \param full_year 1 = DD-MM-YYYY, 0 = DD-MM-YY
 */
void getDateString(char *datestr, int full_year)
{
	// Get current date from RTC
	tm gmt;
	X12RtcGetClock(&gmt);
	
	char year[6];
	char month[3];
	char day[3];

	// Save the parts of the time into chars
	sprintf(year, "%02d", gmt.tm_year + (full_year ? YEAR_FORMAT_FULL : YEAR_FORMAT_SHORT));
	sprintf(month, "%02d", gmt.tm_mon + 1);
	sprintf(day,"%02d", gmt.tm_mday);		
	
	// Create one big string with the full-length string
	strcpy(datestr, day);
	strcat(datestr, "-");
	strcat(datestr, month);
	strcat(datestr, "-");
	strcat(datestr, year);
}

/*!
 * \Sets the given time to the time chip
  */
int setTime(int sec, int min, int hour)
{
	// Get the time from the RTC and store it in a timemeasurement (mt)
	tm gmt;
	X12RtcGetClock(&gmt);
	
	// Change the mt to the new values
	gmt.tm_hour = hour;
	gmt.tm_min = min;
	gmt.tm_sec = sec;
	
	// Set the tm back into the RTC
	return X12RtcSetClock(&gmt);
}

/*!
 *\Sets the given date to the time chip
 */
int setDate(int day, int month, int year, int wDay)
{
	// Get the time and date from the RTC and store it in a timeMeasurement (mt)
	tm gmt;
	X12RtcGetClock(&gmt);
	
	// Change the mt to the new values
	gmt.tm_mday = day;
	gmt.tm_mon = month - 1;
	gmt.tm_year = year - 1900;
	gmt.tm_wday = wDay;
	
	// Set the tm back into the RTC
	return X12RtcSetClock(&gmt);
}

/*!
 *\Gets the current time
 * In the case reading fails, the values will be -1
 */
void getTime(int *sec, int *min, int *hour)
{
	tm gmt;
	if(X12RtcGetClock(&gmt) == 0)
	{ 
		// Set RTC values to pointer variables
		*sec = gmt.tm_sec;
		*min = gmt.tm_min;
		*hour = gmt.tm_hour;
	}
	else /* When reading fails */
	{
		*sec = SAVE_FAIL_VALUE;
		*min = SAVE_FAIL_VALUE;
		*hour = SAVE_FAIL_VALUE;
	}
}

void getCurrentDayOfWeek(int *wDay)
{
	tm gmt;
	if(X12RtcGetClock(&gmt) == 0)
	{
		// Set and convert RTC values to real values
		*wDay = gmt.tm_wday;
	}
	else /* When reading fails */
	{
		*wDay = SAVE_FAIL_VALUE;
	}
}

/*!
 *\Gets the current date
 * In the case reading fails, the values will be -1
 */
void getDate(int *day, int *month, int *year)
{
	tm gmt;
	if(X12RtcGetClock(&gmt) == 0)
	{ 
		// Set and convert RTC values to real values
		*day = gmt.tm_mday + DAY_FORMAT;
		*month = gmt.tm_mon + MONTH_FORMAT;
		*year = gmt.tm_year + YEAR_FORMAT_FULL;
	}
	else /* When reading fails */
	{
		*day = SAVE_FAIL_VALUE;
		*month = SAVE_FAIL_VALUE;
		*year = SAVE_FAIL_VALUE;
	}
}


