/*
 * rss.c
 *
 * Created: 19-3-2019 16:16:24
 *  Author: Yannick
 */ 

#include <sys/thread.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include "Control/util.h"
#include "Control/internetutils.h"
#include "Control/log.h"
#include "Control/shoutcast.h"

// Defining the elements of the feed URL
#define RSS_FEED_TWEAKERS_NEWS	"http://feeds.feedburner.com/tweakers/nieuws"
#define RSS_FEED_GET	"/tweakers/nieuws"
#define RSS_FEED_IP		"172.217.17.78"
#define RSS_FEED_HOST	"feeds.feedburner.com"
#define RSS_FEED_DATA_BUFFER 17

#define OK 0
#define NOK -1

int getLatestRssItem(char* title, char* content)
{
	if(!testInetConnection())
	{
		printf("FUNCTION WERKT NOG NIET TODO");
	}
	return 1;
}

const char* getLatestRssTitle(void)
{
	char* buff;
	
	TCPSOCKET *sock;
	FILE *rssStream;
	
	sock = NutTcpCreateSocket();
	
	if( NutTcpConnect(sock, inet_addr(RSS_FEED_IP), 80) != OK)
	{
		int errorCode = NutTcpError(sock);
		
		printf("\n-- Error in RSS: %d", errorCode);
		//LogMsg_P(LOG_ERR, PSTR("Error in RSS: %d"), errorCode);
		exit(1);
	}
	
	// Prep stream
	rssStream = _fdopen( (int) sock, "r+b");
	
	// Send a header up the stream
	fprintf(rssStream, "GET %s HTTP/1.1\r\n", RSS_FEED_GET);
	fprintf(rssStream, "Host: %s\r\n", RSS_FEED_HOST);
	fprintf(rssStream, "User-Agent: Ethernut\r\n");
	fprintf(rssStream, "Accept: */*\r\n");
	fprintf(rssStream, "Icy-MetaData: 1\r\n");
	fprintf(rssStream, "Connection: close\r\n\r\n");
	fflush(rssStream);
	
	// Set options
	u_short tcpbufsiz = 8760;
	NutTcpSetSockOpt(sock, SO_RCVBUF, &tcpbufsiz, sizeof(tcpbufsiz));
	//Set read timeout of 3 seconds. After that, the stream will read 0
	u_long rx_to = 3000;
	NutTcpSetSockOpt(sock, SO_RCVTIMEO, &rx_to, sizeof(rx_to));
	
	// Server sends a header and xml data (rss formatted) back
	buff = (char*) malloc(512 * sizeof(char));
	
	// Ready to catch the buffer with the first news title
	char* databuff = (char*) malloc(512 * sizeof(char));
	int buffcount = 0;
	
	while( fgets(buff, 512, rssStream) ) /* Receiving the string in 511 char buffers (+ null terminator) */
	{
		if( *buff == 0 ) /* Stop reading if no data comes through */
		{
			break;
		}

		buffcount++;
		
		if(buffcount == RSS_FEED_DATA_BUFFER) /* the buffer that will contain the title*/
		{
			strcpy(databuff, buff);
			break; /* The rest of the stream is not needed */
		}		
	}
	
	// Print the buffer with the title in the log
	printf("\n\n Full data: %s\n Title only: ", databuff);
	
	free(buff);

	
	//Strip the unnesesary data from the title
	
	const char* titleStrippedLeft = strstr(databuff, "<title>") + 7; /* Remove the part up to <title> */
	
	int titleLength = 0;
	char startOfRemoval[9] = "</title>";

	// Get the length of the title	
	char *temp = strstr(titleStrippedLeft, startOfRemoval);
	titleLength = temp - titleStrippedLeft;
	char* titleStrippedFull = (char*) malloc((titleLength + 1) * sizeof(char));
	
	int c = 0;
	
	while (c < titleLength)
	{
		titleStrippedFull[c] = titleStrippedLeft[c];
		c++;
	}
	
	printf(titleStrippedFull);
	
	//// Returning title

	free(databuff);

	return titleStrippedFull;
}

void getTitleFromBuffer(char *buffer, char *dest) /* Doesn't work */
{
	// Strip everything before the title (<title> and everything before that)
	const char* workingTitle = strstr(buffer, "<title>") + 7;
	
	// Strip everything after the title (</title> and everything after that)
	int titleLength = 0;
	char startOfRemoval[9] = "</title>";
	
	char *temp = strstr(workingTitle, startOfRemoval);
	titleLength = temp - workingTitle;
	char title[100];
	
	int counter = 0;
	while(counter < titleLength)
	{
		title[counter] = workingTitle[counter-1];
		counter++;
	}
	
	*dest = *title;
	
}
