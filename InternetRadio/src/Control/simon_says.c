/*
 * simon_says.c
 *
 * Created: 19/03/2019 12:32:58
 *  Author: Tim van der Vooren
 */
#define LOG_MODULE  LOG_MAIN_MODULE
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/thread.h>
#include <sys/timer.h>

#include "Application/menu.h"

#include "Control/log.h"
#include "Control/display.h"
#include "Control/simon_says.h"

#include "Hardware/keyboard.h"
#include "Hardware/vs10xx.h"

int pattern[MAX_SIZE_PATTERN];
int level = 0;
int gameStarted = 0;

int currentPosition = 0;

//this array holds the sounds corresponding to the buttons 1 to 5
int sounds[] = {SOUND_1, SOUND_2, SOUND_3, SOUND_4, SOUND_5};

//this function shows the main screen on the LCD module
void showSimonSaysMainScreen(void)
{
	gameStarted = 0;
	LcdWriteStringLine1("                               ");
	LcdWriteStringLine1("   Simon Says   ");
	LcdWriteStringLine2("  [Ok] = start  ");
}

//this function shows the level up screen on the LCD module
void showLevelUpScreen(void)
{
	playVictorySound();
	LcdWriteStringLine1("  Well Done :)  ");
	char *numberString = "";
	sprintf(numberString, "Finished level %i", level);
	LcdWriteStringLine2(numberString);
	
	NutSleep(BIG_DELAY);
}

//this function shows the victory screen on the LCD module
void showVictoryScreen(void)
{
	playVictorySound();
	resetGame();
	LcdWriteStringLine1("Congratulations!");
	LcdWriteStringLine2("You won the game");
	
	NutSleep(BIG_DELAY);
	showSimonSaysMainScreen();
}

//this function shows the game over screen on the LCD module
void showGameOverScreen(void)
{
	playGameOverSound();
	LcdWriteStringLine1("  GAME OVER :(  ");
	char *numberString = "";
	sprintf(numberString, "You made level %i", level);
	LcdWriteStringLine2(numberString);
	resetGame();
	
	NutSleep(BIG_DELAY);
	showSimonSaysMainScreen();
}

//this function plays the victory sound
void playVictorySound(void)
{
	VsPlayerInit();
	VsBeep(1, 1000);
	VsPlayerInit();
	VsBeep(10, 1000);
	VsPlayerInit();
	VsBeep(100, 1000);
	VsPlayerInit();
	VsBeep(200, 1000);
}

//this function plays the game over sound
void playGameOverSound(void)
{
	VsPlayerInit();
	VsBeep(200, 1000);
	VsPlayerInit();
	VsBeep(100, 1000);
	VsPlayerInit();
	VsBeep(10, 1000);
	VsPlayerInit();
	VsBeep(1, 1000);
}

//this function generates a random pattern that the player has to remember
void randomizePattern(void)
{
	level++;
	int i;
	for(i = 0; i < level*3; i++)
	{
		int randomNumber = 0;
		while(randomNumber == 0)
		{
			randomNumber = rand() % 6;		//generate a random number from 1 to 5
		}
		pattern[i] = randomNumber;
		
		char *levelString = "";
		sprintf(levelString, "    Level %i:    ", level);
		LcdWriteStringLine1(levelString);	//write the current level to the LCD module
		
		char *numberString = "";
		sprintf(numberString, "        %i       ", pattern[i]);
		LcdWriteStringLine2(numberString);	//write the next number from the pattern to the LCD module
		numberString = "";
		
		VsPlayerInit();
		VsBeep(sounds[pattern[i]-1], 100);	//play the sound that corresponds to the next number from the pattern
		
		NutSleep(MEDIUM_DELAY);
		LcdWriteStringLine2("                ");
		NutSleep(SMALL_DELAY);
	}
	
	for(;i < MAX_SIZE_PATTERN-(level*3); i++)
	{
		pattern[i] = 0;		//fill the rest of the array with zeros
	}
	LcdWriteStringLine1("   Your turn:   ");
	LcdWriteStringLine2("                ");
}

//this function resets the variables so that the game can be played again
void resetGame(void)
{
	level = 0;
	int i;
	for(i = 0;i < MAX_SIZE_PATTERN; i++)
	{
		pattern[i] = 0;		//clear the entire array so that it no longer holds the previous pattern
	}
	currentPosition = 0;
}

//this function decides what to do with the key that is pressed
void handleKey(int key)
{
	LcdWriteStringLine1("   Your turn:   ");
	char *keyString = "";
	sprintf(keyString, "        %i       ", key);
	LcdWriteStringLine2(keyString);
	
	int gameOver = 0;
	if(key != pattern[currentPosition])
	{
		gameOver = 1;		//if the key does not correspond with the pattern, it's game over
	}
	currentPosition++;
	
	NutSleep(SMALL_DELAY);
	LcdWriteStringLine2("                ");
	
	if(pattern[currentPosition] == 0)
	{
		showLevelUpScreen();
		if(level != MAX_LEVEL)
		{
			randomizePattern();		//generate a new pattern for the next level
		}
		else
		{
			showVictoryScreen();	//the player has won the game
		}
		currentPosition = 0;
	}
	
	if(gameOver)
	{
		showGameOverScreen();
	}
}

//this function handles the Simon Say's application
THREAD(SimonThread, arg)
{
	int key = GetKeyFound();
	
	srand(time(0));		//this initialization makes sure the pattern is random every time you start up the game
	
	showSimonSaysMainScreen();	//show the main screen on start-up
	while(1)
	{
		switch(key)
		{
			case KEY_OK:
				if(gameStarted != 1)
				{
					randomizePattern();		//start the game with the first randomized pattern
					gameStarted = 1;
				}
				break;
			case KEY_ESC:
				if(gameStarted == 1)
				{
					showSimonSaysMainScreen();	//go back to the home screen
					resetGame();
				}
				else
				{
					stopSimonSays();			//stop the game and resume the menuthread in menu.c
					NutThreadExit();
				}
				break;
			default:
				if(key > 0 && key < 6)
				{
					VsPlayerInit();
					VsBeep(sounds[key-1], 100);	//play the sound that corresponds to the next number from the pattern
					if(gameStarted)
					{
						handleKey(key);
					}
				} 
				else if(key != NO_KEY_INPUT)
				{
					playGameOverSound();	//play the game over sound when a button is pressed that doesnt do anything
				}
				break;
		}
		key = GetKeyFound();
		NutSleep(200);
	}
}

//this function starts the Simon Say's thread
void startSimonSaysThread()
{
	NutThreadCreate("SimonThread", SimonThread, NULL, 512);
	LogMsg_P(LOG_INFO, PSTR("Started Simon Says Thread"));
}
