
/*
 * webinterface.c
 * IPR Web Interface. 
 * Description: Change settings through a web interface using NodeJs and Express.
 * Created: 21-3-2019 09:10:26
 *  Author: Tom Martens
 */ 

#define LOG_MODULE  LOG_SHOUTCAST_MODULE

#include <dev/nicrtl.h>
#include <arpa/inet.h>
#include <sys/confnet.h>
#include <pro/dhcp.h>
#include <sys/socket.h>
#include <string.h>

#include "Control/log.h"
#include "Application/menu.h"
#include "Control/timeutils.h"

#define NUM_OF_SETTINGS 5 //Amount of settings we have that we can change
#define LINE_CHAR_SIZE 15 //How long is the maximum size of an char array that one line containing information could be.
#define TEMP_STORAGE 40 //Storing the variables which we are about to change.
#define PLACEHOLDER_STORAGE 40 //Storage for final parsing

FILE *stream; //Stream for retrieved data
TCPSOCKET *sock; //Socket for connection


/*
 * A function which retrieves data from a self-hosted website
 * Returns -1 on error (could not connect to server)
 * Or returns 1 on success (went through the process without issues)
 */
int connectToWebInterface(void) 
{ 
    int timeOutValue = 5000; //Timeout value for tcp connection
    int returnBuffer = 8192; //Buffer value for tcp connection
	char *line;
	
	//Server settings
	char serverIp[] = "145.49.29.219"; //Server IP
	char webUrl[] = "/data"; //Server URL
	int serverPort = 3000; //Server port
	
	
    sock = NutTcpCreateSocket(); 
 
	//Set time-out value
    NutTcpSetSockOpt(sock, SO_RCVTIMEO, &timeOutValue, sizeof(timeOutValue)); 
	//Set return buffer size
    NutTcpSetSockOpt(sock, SO_RCVBUF, &returnBuffer, sizeof(returnBuffer)); 
 
	//Testing server connection
    int connected = NutTcpConnect(sock,inet_addr(serverIp),serverPort); 
	//Error connecting to the webserver, exiting the function
	if(connected == -1)
	{
		return -1;
	}
    stream = _fdopen((int) sock, "r+b"); 
 
	//Sending HTTP GET request to retrieve data from the website
    fprintf(stream, "GET %s HTTP/1.0\r\n", webUrl); 
    fprintf(stream, "Host: %s\r\n", serverIp); 
    fprintf(stream, "User-Agent: Ethernut\r\n"); 
    fprintf(stream, "Accept: /\r\n"); 
    fprintf(stream, "Connection: close\r\n\r\n"); 
    fflush(stream); 

	
	//Initiating parsing variable, used for tracking position of the temp char array
    int arrayPosition = 0;
 
    //Used for storing the values we found
    char temp[TEMP_STORAGE];
 
    //Storage of the current line
    line = (char *) malloc(LINE_CHAR_SIZE * sizeof(char));
	
    /*
     * Parses a stream (HTML/TXT) into usable data, uses a custom made parser.
     * Every line contains data about the setting in the following way:
     * [100] <- First element, value = 100 (values may vary)
     * [2] <- Second element, value = 2 (values may vary)
	 * [1970-01-01] <- third element  (value may vary)
     * The following code parses these lines into usable values
     */
	int beginning = 0;
	
	while( fgets(line, LINE_CHAR_SIZE, stream) )
	{
		//Are we currently between brackets?
		int inBrackets = 0;
		
		//For the length of the line, let's check every character
		int lineSize = strlen(line);
		int b;
		for(b = 0; b < lineSize ;b++)
		{
			if(line[b] == '%')
			{
				beginning += 1;
				if(beginning == 2)
				{
					//We've reached the end
					break;
				}
			}
			if(beginning == 1) {
			//We're between brackets
			if(inBrackets == 1)
			{
				//Track position of where we are on the line
				arrayPosition += 1;
				//We've found the exit bracket
				if(line[b] == ']')
				{
					//We're out of the brackets
					inBrackets = 0;
					//Put an symbol between the current values, we need this to split the values later on
					temp[arrayPosition] = '@';
					break;
				}
				else
				{
					//We're still in the brackets, add the value to the char array which contains all the values and symbols
					temp[arrayPosition] = line[b];
				}
			}
			else if(line[b] == '[')
			{
				//We're between the brackets now
				inBrackets = 1;
			}
			}
		}
	}
	
	//Close the stream, we're done with this
	free(line); 
	fclose(stream);
	    
	//int for tracking where we current are in dataArr
	int dataTracker = 0;

	//First value: Volume level -> int. Example: 10
	int retrievedVolume = -1;
	//Second value: Standby time -> int. Example: 10
	int retrievedStandbyTime = -1;
	//Third value: Time -> string, got to be converted. Example: 05:55
	//Size is 6, no larger/smaller amount for catch the char array.
	char retrievedTime[6];
	//Fourth value: Date -> string, got to be converted. Example: 1970-01-01
	//Size is 12, no larger/smaller amount for catch the char array.
	char retrievedDate[12];
	//Fifth value: Alarm time -> string, got to be converted. Example: 05:55
	char retrievedAlarm[6];
	//Sorry, no MAC-adres -> too much work on the front-end (website)
	
	//Variable for the loop
	int z = 0;
	//Variable to store char to int array conversion
	int charToInt = 0;
	//Variable to store char to int array conversion, perhaps obsolete, but requires just in case
	int charToIntTwo = 0;
	for(z = 1; z < arrayPosition; z++)
	{
		//Check if the value isn't empty
		if(temp[z] != '\0')
		{
			//Variable to check temp array position
			int tempArrayPos = 0;
			
			char tempArr[PLACEHOLDER_STORAGE];
			while(temp[z] != '@')
			{
				tempArr[tempArrayPos] = temp[z];
				z += 1;
				tempArrayPos += 1;
			}
			//We've found variables to parse
			if(tempArrayPos > 0)
			{
				switch(dataTracker)
				{
					case 0 : 
						//Turning integer char arrays into actual values { '4','5','2' } = 452
						sscanf(tempArr, "%d", &charToInt);
						retrievedVolume = charToInt;
					break;
					case 1 :
						sscanf(tempArr, "%d", &charToIntTwo);
						retrievedStandbyTime = charToIntTwo;
					break;
					case 2 :
						strcpy(retrievedTime, tempArr);
					break;
					case 3 :
						strcpy(retrievedDate, tempArr);
					break;
					case 4 :
						strcpy(retrievedAlarm, tempArr);
					break;
						
				}
				//Changing position so the next value can be assigned.
				dataTracker += 1;
			}
			
			//We need this to clear the array, otherwise the values can get bugged by previous values, assigning random value (Bug with IPAC?)
			int v;
			for(v = 0; v < PLACEHOLDER_STORAGE; v++)
			{
				tempArr[v] = 0;
			}
		}
	}
	//Setting volume
	setVolume(retrievedVolume);
	
	//Setting standby time
	setStandbyTime(retrievedStandbyTime);
	
	
	//Converting char array to time (Example: 09:44)
	int h1 = retrievedTime[0] - '0';
	int h2 = retrievedTime[1] - '0';
	int h3 = h1 * 10 + h2;
	
	int m1 = retrievedTime[3] - '0';
	int m2 = retrievedTime[4] - '0';
	int m3 = m1 * 10 + m2;
	setTime(0,m3,h3);
	
	//Converting char array to alarm time (Example: 09:44)
	int alarm_h1 = retrievedAlarm[0] - '0';
	int alarm_h2 = retrievedAlarm[1] - '0';
	int alarm_h3 = alarm_h1 * 10 + alarm_h2;
		
	int alarm_m1 = retrievedAlarm[3] - '0';
	int alarm_m2 = retrievedAlarm[4] - '0';
	
	setAlarmTimeIPR(alarm_h3,alarm_m1,alarm_m2);
	
	
	//Converting (YYYY-MM-DD) to day, month, years
	int dateYear1 = retrievedDate[0] - '0';
	int dateYear2 = retrievedDate[1] - '0';
	int dateYear3 = retrievedDate[2] - '0';
	int dateYear4 = retrievedDate[3] - '0';
	
	int dateYearTotal = dateYear1 * 1000 + dateYear2 * 100 + dateYear3 * 10 + dateYear4;
	
	int dateMonth1 = retrievedDate[5] - '0';
	int dateMonth2 = retrievedDate[6] - '0';
	int dateMonthTotal = dateMonth1 * 10 + dateMonth2;
	
	int dateDay1 = retrievedDate[8] - '0';
	int dateDay2 = retrievedDate[9] - '0';
	int dateDayTotal = dateDay1 * 10 + dateDay2;
	
	setNewDate(dateDayTotal,dateMonthTotal,dateYearTotal);

	//Everything went well, returning 1
	return 1;
} 
