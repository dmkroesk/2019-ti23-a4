/*
 * flashIO.c
 *
 * Created: 12-3-2019 11:52:37
 *  Author: rickw
 */ 
#define LOG_MODULE  LOG_MAIN_MODULE

#include "Control/log.h"

#include "Hardware/flash.h"

//this function saves the mac address to the flash chip of the IPAC
void saveMacAddressToChip(int data, int page)
{	
	int macBuffer[] = {data};
	At45dbChipErase();									// erase whole flash chip
	At45dbPageWrite(page, &macBuffer[0], 4);			// write macAddress to page 10-15 of flash chip
}

//this function reads the mac address from the flash chip of the IPAC
void readMacAddress(int *data, int page)
{
	int staticAddress[1];
	At45dbPageRead(page, staticAddress, 4);
	*data = staticAddress[0];
}

//this function saves the standby time to the flash chip of the IPAC
void saveStandbyTimeToChip(int data)
{
	int standbyBuffer[] = {data};
	At45dbChipErase();								// erase whole flash chip
	At45dbPageWrite(0, &standbyBuffer[0], 4);		// write standbyTime to page 0 of flash chip
}

//this function reads the standby time from the flash chip of the IPAC
void readStandbyTime(int *data)
{
	int savedStandbyTime[1];
	At45dbPageRead(0, savedStandbyTime, 4);			// read value from flash memory page 0 and set savedStandbyTime to the value
	*data = savedStandbyTime[0];
}

//this function saves the alarm time to the flash chip of the IPAC
void saveAlarmTimeToChip(int data)
{
	int alarmBuffer[] = {data};
	At45dbChipErase();								// erase whole flash chip
	At45dbPageWrite(17, &alarmBuffer[0], 4);		// write alarmTime to page 17 of flash chip
}

//this function readse the alarm time from the flash chip of the IPAC
void readAlarmTime(int *data)
{
	int savedAlarmTime[1];
	At45dbPageRead(17, savedAlarmTime, 4);			// read value from flash memory page 17 and set savedAlarmTime to the value
	*data = savedAlarmTime[0];
}


