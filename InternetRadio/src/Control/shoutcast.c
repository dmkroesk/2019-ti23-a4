/*
 * shoutcast.c
 *
 * Created: 26-2-2019 15:33:09
 *  Author: Tom Martens
 */ 
#define LOG_MODULE  LOG_SHOUTCAST_MODULE

#include <sys/thread.h>
#include <sys/timer.h>

#include <dev/nicrtl.h>
#include <arpa/inet.h>
#include <sys/confnet.h>
#include <pro/dhcp.h>
#include <sys/socket.h>

#include "Control/internetutils.h"
#include "Control/log.h"
#include "Control/shoutcast.h"
#include "Control/player.h"
#include "Control/userdata.h"
#include "Control/display.h"

#include "Hardware/vs10xx.h"

#define ETH0_BASE	0xC300
#define ETH0_IRQ	5

#define OK			0
#define NOK			-1

static char eth0IfName[9] = "eth0";
FILE *stream;

int testInetConnection(void)
{
	TCPSOCKET *sock;
	sock = NutTcpCreateSocket();
					
	if (NutTcpConnect(sock, inet_addr("172.217.17.36"), 80))
	{
		LogMsg_P(LOG_INFO, PSTR("Internet connection ERROR"));
		return NOK;
	}
	else
	{
		LogMsg_P(LOG_INFO, PSTR("Internet connection OK"));
		return OK;
	}
}

//this function writes a custom character to the standby menu which represents the current internet status
void writeInternetStatusToDisplay(int inetStatus)
{
	LcdWriteStringLine2("[Esc] Menu     ");
	if(inetStatus == OK)
	{
		LcdWriteCustomCharSuccess();
	}
	else
	{
		LcdWriteCustomCharFail();
	}
}

int connectToStream(USERDATA_STRUCT newStream)
{
	int result = OK;
	char *data;
	TCPSOCKET *sock;
	sock = NutTcpCreateSocket();
	
	if( NutTcpConnect(sock, inet_addr(newStream.ip), newStream.port) != 0)
	{
		int errorCode = NutTcpError(sock);
		
		LogMsg_P(LOG_ERR, PSTR("Error: >> NutTcpConnect()%d"), errorCode);
		exit(1);
	}
	stream = _fdopen( (int) sock, "r+b" );
	
	fprintf(stream, "GET %s HTTP/1.0\r\n", "/");
	fprintf(stream, "Host: %s\r\n", inet_addr(newStream.ip));
	fprintf(stream, "User-Agent: Ethernut\r\n");
	fprintf(stream, "Accept: */*\r\n");
	fprintf(stream, "Icy-MetaData: 1\r\n");
	fprintf(stream, "Connection: close\r\n\r\n");
	fflush(stream);

	u_short tcpbufsiz = 8760;
	NutTcpSetSockOpt(sock, SO_RCVBUF, &tcpbufsiz, sizeof(tcpbufsiz));
	//Set read timeout of 3 seconds. After that, the stream will read 0
	u_long rx_to = 3000;
	NutTcpSetSockOpt(sock, SO_RCVTIMEO, &rx_to, sizeof(rx_to));

	// Server stuurt nu HTTP header terug, catch in buffer
	data = (char *) malloc(512 * sizeof(char));
	
	while( fgets(data, 512, stream) )
	{
		if( 0 == *data )
			break;

		printf("%s", data);
	}
	
	free(data);
	result = OK;
	
	return result;
}

int playStream(void)
{
	play(stream);
	
	return OK;
}

int stopStream(void)
{
	VsPlayerStop();
	
	setCloseStream(1);
	
	NutSleep(1000);
	
	printf("Cleaned the pipe");
	
	fclose(stream);	
	
	return OK;
}
