/* 
 * alarm.c 
 * 
 * Created: 19-3-2019 12:35:45 
 *  Author: rickw 
 */  
#define LOG_MODULE  LOG_MAIN_MODULE 
#include <stdio.h> 
#include <time.h>

#include "Control/log.h"

#include "Hardware/vs10xx.h"
#include "Hardware/flash.h"
 
static int page = 30; 
 
void addAlarm(int day, int totalAlarmMinutes) 
{ 
	int saveAlarm[] = {totalAlarmMinutes}; 

	At45dbChipErase();	 
	At45dbPageWrite(page + day, &saveAlarm[0], 4); 
}
 
void readDailyAlarm(int *alarms, int day) 
{ 
	int alarmTimesBuffer[1]; 		 
	At45dbPageRead(page + day, alarmTimesBuffer, 4); 
	*alarms = alarmTimesBuffer[0]; 
} 
