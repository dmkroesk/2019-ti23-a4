/*
 * menu.h
 *
 * Created: 19/02/2019 11:07:29
 *  Author: Tim van der Vooren
 */
#ifndef MENU_H_
#define MENU_H_

//MENU ID's
#define MENU_STANDBY 0

#define MENU_MAIN_1_ID 1				//Modus
#define MENU_MAIN_2_ID 2				//Instellingen
#define MENU_MAIN_3_ID 3				//Simon Says's (Tim IPR)

#define MODUS_SUB_1_ID 11				//Modus - Wekker
#define MODUS_SUB_2_ID 12				//Modus - Radio
#define MODUS_SUB_3_ID 13				//Modus - RSS feed

#define SETTINGS_SUB_1_ID 21			//Instellingen - Wekker
#define SETTINGS_SUB_2_ID 22			//Instellingen - Radio
#define SETTINGS_SUB_3_ID 23			//Instellingen - Netwerk
#define SETTINGS_SUB_4_ID 24			//Instellingen - Tijd
#define SETTINGS_SUB_5_ID 25			//Instellingen - Datum
#define SETTINGS_SUB_6_ID 26			//Instelling IPR webinterface

#define ALARM_SUB_1_ID 211				//Wekker - Dagelijks alarm
#define ALARM_SUB_2_ID 212				//Wekker - Alarm per dag
#define ALARM_SUB_SUB_1_1_ID 2111		//Dagelijks alarm - Tijd
#define ALARM_SUB_SUB_2_1_ID	2121	//Alarm per dag - Maandag
#define ALARM_SUB_SUB_2_2_ID	2122	//Alarm per dag - Dinsdag
#define ALARM_SUB_SUB_2_3_ID	2123	//Alarm per dag - Woensdag
#define ALARM_SUB_SUB_2_4_ID	2124	//Alarm per dag - Donderdag
#define ALARM_SUB_SUB_2_5_ID	2125	//Alarm per dag - Vrijdag
#define ALARM_SUB_SUB_2_6_ID	2126	//Alarm per dag - Zaterdag
#define ALARM_SUB_SUB_2_7_ID	2120	//Alarm per dag - Zondag
#define ALARM_SUB_SUB_TIME_2_1_1_ID 21211 //Alarm per dag - Tijd
#define RADIO_SUB_1_ID 221				//Radio - Jazz
#define RADIO_SUB_2_ID 222				//Radio - Classic rock
#define RADIO_SUB_3_ID 223				//Radio - Aardschock
#define RADIO_SUB_4_ID 224				//Radio - Slam.fm
#define RADIO_SUB_5_ID 225				//Radio - Qmusic

#define NETWORK_SUB_1_ID 231			//Netwerk - MacAddress

#define TIME_SUB_1_ID 241				//Tijd - Tijd
#define TIME_SUB_2_ID 242				//Tijd - Tijdzone
#define TIME_SUB_3_ID 243				//Tijd - Standby

#define TIME_SUB_SUB_1_1_ID 2411		//Tijd - Tijd detail
#define TIME_SUB_SUB_2_1_ID 2421		//Tijd - Tijdzone detail
#define TIME_SUB_SUB_3_1_ID 2431		//Tijd - Standby detail

#define DATE_SUB_1_ID 251				//Datum - Datum
#define WEB_SUB_1_ID 261				//IPR webinterface hoofdscherm

#define VOLUME_UP 61
#define VOLUME_DOWN 62
#define RSS_FEED				131		// RSS feed

//MENU KEY ID's
#define MENU_KEY_ESC	0
#define MENU_KEY_OK		1
#define MENU_KEY_LEFT	2
#define MENU_KEY_RIGHT	3
#define MENU_KEY_UP		4
#define MENU_KEY_DOWN	5

typedef struct {
	unsigned int id;					/* ID for this item */
	unsigned int newId[6];				/* IDs to jump to on keypress */
	char *text[2];						/* Text for this item */
	void (*keyAction[6])(void);
} MENU_ITEM;

extern void WriteLinesToLcd(char *lines[]);
extern void HandleMenu(int key);
//extern int menuThread(void);
extern void setTimeInMenu(int hours, int minutes);
extern void volumeUp(void);
extern void volumeDown(void);
extern void power(void);

void setCursorPositionLeft(void);
void setCursorPositionRight(void);

void setCurrentAlarmTimeToDisplay(void);

void setStandbyTimeToDisplay(void);
void increaseStandbyTime(void);
void decreaseStandbyTime(void);
void saveStandbyTime(void);
void setStandbyTime(int data);

void saveTimeSettings(void);
void increaseTime(void);
void decreaseTime(void);
void updateSetTimeDisplay(void);
void setCurrentTimeToDisplay(void);

void saveDateSettings(void);
void increaseDate(void);
void decreaseDate(void);
void updateSetDateDisplay(void);
void setCurrentDateToDisplay(void);

void saveAlarmTime(void);

void updateSetMacAddressDisplay(void);
void increaseMacAddress(void);
void decreaseMacAddress(void);
void saveMacAddressSettings(void);
void saveMacAddress(void);
void setCurrentMacAddressToDisplay(void);

void changeRadioStation(void);
void startRadioStream(void);
void stopRadioStream(void);

void webinterfaceMenu(void);

void checkStandbyMode(void);
void startSimonSays(void);
void stopSimonSays(void);
void startMenuThread(void);

void setNewDate(int dayNew, int monthNew, int yearNew);
void setAlarmTimeIPR(int hours, int minutesFirst, int minutesSecond);
void setVolume(int data);

void SetDayAlarm(void);
void SetCurrentDayID(void);
#endif /* MENU_H_ */
