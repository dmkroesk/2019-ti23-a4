/* ========================================================================
 * [PROJECT]    SIR
 * [MODULE]     Remote Control
 * [TITLE]      remote control header file
 * [FILE]       remcon.h
 * [VSN]        1.0
 * [CREATED]    1 july 2003
 * [LASTCHNGD]  1 july 2003
 * [COPYRIGHT]  Copyright (C) STREAMIT BV 2010
 * [PURPOSE]    remote control routines for SIR
 * ======================================================================== */

/*-------------------------------------------------------------------------*/
/* global defines                                                          */
/*-------------------------------------------------------------------------*/
#define RC_OK                 0x00
#define RC_ERROR              0x01
#define RC_BUSY               0x04

#define RCST_IDLE             0x00
#define RCST_WAITFORLEADER    0x01
#define RCST_SCANADDRESS      0x02
#define RCST_SCANDATA         0x03

#define RC_INT_SENS_MASK      0x03
#define RC_INT_FALLING_EDGE   0x02
#define RC_INT_RISING_EDGE    0x03

#define IR_RECEIVE            4
#define IR_BUFFER_SIZE        1

// All RemoteControl buttons mapped with their corresponding value
#define RC_BUTTON_ESC       16 //POWER 
#define RC_BUTTON_UP		0 //CH+
#define RC_BUTTON_LEFT		192//VOL-
#define RC_BUTTON_RIGHT		64//VOL+
#define RC_BUTTON_DOWN		128//CH-
#define RC_BUTTON_OK		144//MUTE
#define RC_BUTTON_N1		8//1
#define RC_BUTTON_N2		136//2
#define RC_BUTTON_N3		72//3
#define RC_BUTTON_N4		200//4
#define RC_BUTTON_N5		40//5
#define RC_BUTTON_ALT		80//A-B
#define RC_BUTTON_POWER		42//EXIT




/*-------------------------------------------------------------------------*/
/* export global routines (interface)                                      */
/*-------------------------------------------------------------------------*/
void    RcInit(void);
void    printRemoteconInfo(void);
int	RcGetKeyboardKey(void);


/*  ????  End Of File  ???????? ???????????????????????????????????????????? */
