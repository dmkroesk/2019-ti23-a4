/*
 * util.h
 *
 * Created: 22-2-2019 10:57:05
 *  Author: Yannick
 */ 


#ifndef UTIL_H_
#define UTIL_H_

void *MyMalloc(unsigned int unSize);
char *strdup(CONST char *str);
int BufferMakeRoom(char **ppcBuf, unsigned int *punBufSize, unsigned int unBufInUse, unsigned int unSizeNeeded);
int BufferAddString(char **ppcBuf, unsigned int *punBufSize, unsigned int *punBufInUse, CONST char *pszString);
char *replace_str(char *str, char *orig, char *rep);


#endif /* UTIL_H_ */
