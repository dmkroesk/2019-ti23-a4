/*
 * rss.h
 *
 * Created: 19-3-2019 16:22:22
 *  Author: Yannick
 */ 


#ifndef RSS_H_
#define RSS_H_

#include "Control/internetutils.h"
#include "log.h"

// Functions
int getLatestRssItem(char* title, char* content);
const char* getLatestRssTitle(void);
void getTitleFromBuffer(char *buffer, char *dest);


#endif /* RSS_H_ */
