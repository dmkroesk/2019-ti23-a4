/*
 * userdata.h
 *
 * Created: 26-2-2019 15:46:14
 *  Author: Tom Martens
 */ 

#ifndef USERDATA_INC
#define USERDATA_INC

typedef struct
{
	char ip[16];
	int port;
	int volume;
} USERDATA_STRUCT;

int initUserData(void);
int savePersistent(USERDATA_STRUCT *src, int size);
int openPersistent(USERDATA_STRUCT *src, int size);
void showPage(u_long pgn);

void testRomFs(void);

#endif
