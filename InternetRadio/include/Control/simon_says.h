/*
 * simon_says.h
 *
 * Created: 19/03/2019 12:38:23
 *  Author: Tim van der Vooren
 */ 
#define MAX_SIZE_PATTERN 12
#define MAX_LEVEL 4

#define SMALL_DELAY 500
#define MEDIUM_DELAY 800
#define BIG_DELAY 2000

#define SOUND_1 6
#define SOUND_2 8
#define SOUND_3 10
#define SOUND_4 12
#define SOUND_5 14

void showSimonSaysMainScreen(void);
void showLevelUpScreen(void);
void showVictoryScreen(void);
void showGameOverScreen(void);

void playVictorySound(void);
void playGameOverSound(void);

void randomizePattern(void);
void resetGame(void);

void handleKey(int key);
void startSimonSaysThread(void);
