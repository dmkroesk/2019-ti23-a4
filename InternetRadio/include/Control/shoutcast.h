/*
 * shoutcast.h
 *
 * Created: 26-2-2019 15:34:43
 *  Author: Tom Martens
 */ 


#ifndef SHOUTCAST_INC
#define SHOUTCAST_INC

//int initInet(void);
int connectToStream(USERDATA_STRUCT);
int playStream(void);
int stopStream(void);
int testInetConnection(void);
void writeInternetStatusToDisplay(int inetStatus);

#endif
