/*
 * internetutils.h
 *
 * Created: 28-2-2019 15:56:59
 *  Author: Tom Martens
 */ 

#ifndef INTERNETUTILS_H_
#define INTERNETUTILS_H_

#include <time.h>

int initInet(void);
extern int getNTPTime(tm *ntp_time);

#endif /* TIMEUTILS_H_ */
