/*
 * timeutils.h
 *
 * Created: 19-2-2019 16:27:26
 *  Author: Yannick
 */ 


#ifndef TIMEUTILS_H_
#define TIMEUTILS_H_

#define SAVE_FAIL_VALUE -1
#define YEAR_FORMAT_FULL 1900
#define YEAR_FORMAT_SHORT -100
#define MONTH_FORMAT 1
#define DAY_FORMAT 0

#include <stdio.h>
#include <string.h>
#include "Hardware/rtc.h"

extern int setTime(int sec, int min, int hour);
extern int setDate(int day, int month, int year, int wDay);
extern void getCurrentDayOfWeek(int *wDay);
extern void getTime(int *sec, int *min, int *hour);
extern void getDate(int *day, int *month, int *year);
extern void getDateString(char *datestr, int full_year);
extern void getTimeString(char *timestr, int include_seconds);

#endif /* TIMEUTILS_H_ */
