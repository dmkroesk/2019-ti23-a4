/*
 * flashIO.h
 *
 * Created: 12-3-2019 11:53:07
 *  Author: rickw
 */ 
void saveMacAddressToChip(int data, int page);
void readMacAddress(int *data, int page);

void saveStandbyTimeToChip(int data);
void readStandbyTime(int *data);

void saveAlarmTimeToChip(int data);
void readAlarmTime(int *data);

